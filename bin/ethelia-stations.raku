#!/bin/env raku

use JSON::Fast;
use MIME::Base64;

use Ethelia;
use Ethelia::Advancer;
use Ethelia::StationParser::AQICN;
use Ethelia::StationParser::AQICN::Repository::FileChain;
use Ethelia::StationParser::AQICN::Repository::BlockChain;
use Pheix::Model::JSON;
use Pheix::Test::Blockchain;
use Pheix::Test::BlockchainComp::Helpers;

sub MAIN (
    Str  :$token!,
    Str  :$localtable  = 'ExtensionEthelia',
    Str  :$remotetable = 'blockchain/ExtensionEthelia',
    Str  :$localstoragepath = sprintf("%s/git/raku-ethelia/t/custom-config/storage/test", $*HOME),
    Str  :$remoteconfigpath = sprintf("%s/git/raku-ethelia/custom-config/storage/ExtensionEthelia/", $*HOME),
    Str  :$abi      = sprintf("%s/git/dcms-raku/conf/system/eth", $*HOME),
    Bool :$debug    = False,
    Bool :$create   = False,
    Bool :$purge    = False,
    Bool :$unlock   = True,
    Bool :$show     = False,
    Bool :$advance  = False,
    Bool :$blobbing = False,
    Bool :$annotate = False,
    Rat  :$trxdelay = 0.0,
    Str  :$trxlogf,
) {
    my $start = now;

    try {
        X::AdHoc.new(:payload(sprintf("ABI path is not existed %s", $abi))).throw unless $abi.IO.d;

        $*ERR.out-buffer = False;
        $*OUT.out-buffer = False;

        my $abstractprovider = Ethelia::StationParser::AQICN.new;

        my $remoteconf = $abstractprovider.config(:table($remotetable), :path($remoteconfigpath), :overrideconfig({path => $abi}), :build(False));
        my $remoterepo = Ethelia::StationParser::AQICN::Repository::BlockChain.new(:table($remotetable), :config($remoteconf),  :$debug);
        my $localrepo  = Ethelia::StationParser::AQICN::Repository::FileChain
            .new(:$debug, :table($localtable), :config($abstractprovider.config(:table($localtable), :path($localstoragepath))));

        X::AdHoc.new(:payload('no local database')).throw  unless $localrepo.dbobj  && $localrepo.dbobj.dbswitch  == 0;
        X::AdHoc.new(:payload('no remote database')).throw unless $remoterepo.dbobj && $remoterepo.dbobj.dbswitch == 1;

        my $localprovider  = Ethelia::StationParser::AQICN.new(:$token, :dbobj($localrepo.dbobj),  :$debug, :$trxdelay, :$blobbing, :$annotate);
        my $remoteprovider = Ethelia::StationParser::AQICN.new(:$token, :dbobj($remoterepo.dbobj), :$debug, :$trxdelay, :$blobbing, :$annotate);

        return purge(:$start, :$localrepo, :$remoterepo, :$unlock) if $purge;

        if $advance {
            if $trxlogf && $trxlogf.IO.f {
                my @log    = $trxlogf.IO.lines.map({ $_.split(q{|}, :skip-empty).map({ $_.trim }) });
                my @trxlog = @log.map({ %(txhash => $_[4], nonce => $_[1].UInt, caller => $_[5]) });

                my $advancer = Ethelia::Advancer.new(:$debug);
                my %result   = $advancer.advance(:@trxlog, :dbobj($remoterepo.dbobj));

                my Str $message =
                    sprintf(
                        "successfully advanced %d/%d transactions in %.03f seconds",
                        %result<transaction_num>,
                        @trxlog.elems,
                        %result<runtime>);

                notify(:path($remoteconfigpath), :$message);

                return 0;
            }
            else {
                X::AdHoc.new(:payload('no valid trx log file is given via --trxlogf=/path/to/logfile')).throw;
            }
       }

        my @stations = $localprovider.stations;

        X::AdHoc.new(:payload('no stations found')).throw unless @stations.elems;

        if !$localrepo.dbobj.exists && $create {
            X::AdHoc.new(:payload(sprintf("table %s at %s create failure", $localtable, $localstoragepath)))
                unless $localrepo.dbobj.chainobj.table_create<status>;

            sprintf("table %s at %s is created", $localtable, $localstoragepath).say;
        }

        X::AdHoc.new(:payload(sprintf("sync with local table %s at %s is failed", $localtable, $localstoragepath))).throw unless
            $localprovider.synclocal(:@stations);

        my @localdb = $localrepo.select_all;

        X::AdHoc.new(:payload(sprintf("sync with remote table %s at config %s is failed", $remotetable, $remoteconfigpath))).throw unless
            $remoteprovider.syncremote(:@localdb, :dbobj($remoterepo.dbobj));

        X::AdHoc.new(:payload('failure while saving signer log file')).throw unless
            Pheix::Test::BlockchainComp::Helpers
                .new(:tstobj(Pheix::Test::Blockchain.new))
                .flush_signing_session(
                    :sgnlog($remoterepo.dbobj.chainobj.sgnlog),
                    :filepostfix('-aqicn-update')
                );

        X::AdHoc.new(:payload(sprintf("add transaction hashes to local table %s at config %s is failed", $localtable, $localstoragepath))).throw unless
            $localprovider.addhashes(:trxlog($remoteprovider.trxlog.tail), :table($localtable), :path($localstoragepath));

        sprintf("successfully processed %d remote stations (database has %d records)", @stations.elems, @localdb.elems).say;

        my Str $message = sprintf("synced %d transactions via <%s> endpoint in %.03f seconds", $remoteprovider.trxlog.tail.elems, $remoteprovider.get_used_endpoint, now - $start);

        show_localdb(:$localrepo) if $show;

        notify(:path($remoteconfigpath), :$message);

        CATCH {
            default {
                my $e = .message;
                my $b = .backtrace;

                notify(:path($remoteconfigpath), :message(sprintf("[FAILURE]: %s\n%s", $e, $b)));

                X::AdHoc.new(:payload(sprintf("%s died in %.03f seconds", $*PROGRAM-NAME, now - $start))).throw;
            }
        }
    }

    return 0;
}

sub purge(
    Instant :$start = now,
    Ethelia::StationParser::AQICN::Repository::FileChain :$localrepo!,
    Ethelia::StationParser::AQICN::Repository::BlockChain :$remoterepo!,
    Bool :$unlock = True;
) returns UInt {
    if $localrepo.dbobj.exists {
        X::AdHoc.new(:payload(sprintf("drop table %s is failed", $localrepo.dbobj.table))).throw unless
            $localrepo.dbobj.remove_all;
    }

    if $remoterepo.dbobj.exists {
        $remoterepo.dbobj.chainobj.nounlk = True unless $unlock;

        X::AdHoc.new(:payload(sprintf("drop table %s is failed", $remoterepo.dbobj.table))).throw unless
            $remoterepo.dbobj.remove_all;
    }

    sprintf("purged in %.03f seconds", now - $start).say;

    return 0;
}

sub show_localdb(Ethelia::StationParser::AQICN::Repository::FileChain :$localrepo!) {
    my @localdb = $localrepo.select_all.map({
        %(
            id => $_<id>,
            data => MIME::Base64.decode-str($_<data>).split(q{|}, :skip-empty).map({
                my $value = $_;
                try {
                    $value = from-json(MIME::Base64.decode-str($value));
                    CATCH {
                        default {
                            ;
                        }
                    }
                }

                $value;
            }),
            compression => $_<compression>,
        )
    });

    to-json(@localdb.sort({%^a<id>.UInt <=> %^b<id>.UInt})).say;
}

sub notify(Str :$path!, Str :$message!) returns Bool {
    my $jsonobj = Pheix::Model::JSON.new(:$path).set_entire_config(:addon('Pheix'));

    $jsonobj.set_group_setting('Pheix', 'notifier', 'matrix', 'roomid', '!WabhPnObqulbloDQWP:glasgow.social', :temporary(True));
    $jsonobj.set_setting('Pheix', 'defaultnotifier', 'value', 'matrix', :temporary(True));

    $message.say;

    try {
        return Ethelia.new(:$jsonobj).send_notification(:topic('securityupdate'), :payload({message => $message}), :key('message'));

        CATCH {
            default {
                my $e = .message;
                my $b = .backtrace;

                sprintf("[FAILURE] notification is not sent: %s\n%s", $e, $b).say;
            }
        }
    }

    return False;
}
