# Raku Ethelia

**Ethelia** is Ethereum blockchain storage provider for different kinds of events. Visit our official site: https://ethelia.pheix.org/

Basically Ethelia uses [Pheix](https://pheix.org) as middle layer to access the data on Ethereum blockchain. Not as a service, but a framework. Technically Ethelia is a Pheix extension with business and service related logic in a set of independent modules. It uses Pheix features directly via object oriented model — methods and attributes.

Check out the list of [Ethelia related repositories](https://gitlab.com/groups/ethelia/-/shared?sort=name_asc) for more details.

## Testing with `Trove`

You can automate unit tests running with `Trove` [module](https://gitlab.com/pheix/raku-trove):

```bash
RAKULIB=.,$HOME/git/dcms-raku,$HOME/git/pheix-research/raku-dossier-prototype/ trove-cli -c --f=`pwd`/.trove.conf.yml --p=yq
```

## API

Please check full Ethelia [API documentation](https://docs.ethelia.pheix.org/api/auth/) and feel free to go through [Getting Started](https://docs.ethelia.pheix.org/rawhide/gettingstarted/) guide.

### Sample requests

#### Create

```javascript
{
    "credentials": {
        "token": "0x99caaa44ecf6fd6e561a35a8892a92cc91a325c685db95e3c7b2a8b5097966f7"
    },
    "method": "GET",
    "route": "/api/ethelia/event/store",
    "payload": {
        "code": "01982-1",
        "topic": "blockchain_event_1",
        "payload": {
            "return": 1,
            "score": 2022,
            "result": "success",
            "details": {
                "items": [
                    1.87,
                    2.16,
                    3.52
                ],
                "status": true
            }
        }
    }
}
```
```javascript
{
    "credentials": {
        "token": "0x99caaa44ecf6fd6e561a35a8892a92cc91a325c685db95e3c7b2a8b5097966f7"
    },
    "method": "GET",
    "route": "/api/ethelia/event/store",
    "payload": {
        "code": "1004-1",
        "topic": "pm2.5_event_1",
        "payload": {
            "sensorvalue": 24,
            "sensordevice": "Xaomi Air Purifier 4 Lite"
        }
    }
}
```

#### Search

```javascript
{
    "credentials": {
        "token": "0xd78776d2eec8eb2835e86d7c8bb070abf001543171ffd31f676e6997e6f54f33"
    },
    "method": "GET",
    "route": "/api/ethelia/event/search",
    "payload": {
        "search": {
            "target": "1",
            "value": "*"
        }
    }
}
```

#### Decode input transaction data

1. `set`: https://sepolia.etherscan.io/tx/0x75955f529abc5539b8ffeb11c9a08571e08c8054d82f794aea5bcd2379bc4afd
2. `insert`: https://sepolia.etherscan.io/tx/0x6ec5fafed26c3a7d7586f9d8c153e348ff14cd88f1b648434c9f6c03db37ed07

```javascript
{
    "credentials": {
        "token": "0xf97e55f06ff19dd7d4c2da52a289837641d029b92d336e65f9a0c88bb0d2e4ae"
    },
    "method": "GET",
    "route": "/api/ethelia/transaction/decode",
    "payload": {
        "function": "set",
        "inputdata": "0xf609bacf00000000000000000000000000000000000000000000000000000000000000a00000000000000000000000000000000000000000000000000000000065c8fc2e00000000000000000000000000000000000000000000000000000000000000e0000000000000000000000000000000000000000000000000000000000000000100000000000000000000000000000000000000000000000000000000000002a00000000000000000000000000000000000000000000000000000000000000010457874656e73696f6e457468656c696100000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000019d425a6836314159265359ed2e145f00000d1f8040067ff23dffbff03ffffbf43001531b6b1153fd54c869808600832346269a32620da8c0d54fd4f4a3c29e2689b54dea9faa3406d324cd4c40068682535149e6a8f50f29a6d2681e53d4687a4da9ea7a987aa00d09a2f53767c332adf021cefcf5ce16d3ca926bf36cb6e67eae584449a530761ee33c2cb2b92a45027b28ecf27431af09c3a48c18431c004d74e27ac250747b43ae7dece391d0bbba90a509211a055725ba72037a996b781f42be8b2bb8a6331bdadcd6714c636ddca5293f00dac9ac1c84932679b6c84b3fbc187e296f4684c66dc244e5b566cf6f64dd27cf839ef50a0d3218b2971534984b91d18bd0d5b82d3a442919210a8001e678d21b6103da3b0320adf66d6c229f13bf8252fa7a90ea6d7e99631197217eb3eda933b94f76420ba02769a37551e3ab4fda37e6fe112212105e96ff94211edc089d44366c938433e3ae76e6287119cfe359ab51e95d4165bf41e24304eb17ea53c559c29c328f508c97688ebe62e0d0d6f7104ede16be5add2bb3e8ca8c29fd8d6188bb9229c284876970a2f8000000000000000000000000000000000000000000000000000000000000000000008430784443314543364436384337363732373243413834303344353332363532304235313036303545423539333032463833333232383542443634444242413233383843424133393736313133313435413145453745314441373633423231374534424444464332304534423438314233433446363630313237464444393332313437303100000000000000000000000000000000000000000000000000000000"
    }
}
```

## Pre-launch Telegram channel

[![Pre-launch Telegram channel invite](https://gitlab.com/pheix-io/ethelia/-/raw/main/docs/assets/images/prelaunch-telegram-preview.png "Pre-launch Telegram channel invite")](https://t.me/+0YOeSWFoceAyYjBi)

## License information

This is free and opensource software, so you can redistribute it and/or modify it under the terms of the [The Artistic License 2.0](https://opensource.org/licenses/Artistic-2.0).

## Author

Please contact me via [Matrix](https://matrix.to/#/@k.narkhov:matrix.org) or [LinkedIn](https://www.linkedin.com/in/knarkhov/). Your feedback is welcome at [narkhov.pro](https://narkhov.pro/contact-information.html).
