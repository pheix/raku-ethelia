#!/bin/bash

ME=`basename "$0"`
WORKDIR=/home/kostas/git/raku-ethelia
PHEIXLIB=$HOME/nvme/home/apache_root/pheix.org/git/dcms-raku/lib/
ETHELIALIB=$HOME/git/raku-ethelia/lib/
DOSSIERLIB=$HOME/git/pheix-research/raku-dossier-prototype/
DEBUG="False"
DATABASE_LOCAL="local/holesky/ExtensionEthelia"
DATABASE_REMOTE="holesky/ExtensionEthelia"
HOUR=`date +"%H"`
DATABASE=

if [ "$1" == "--debug" ]; then
	DEBUG="True"

	echo "[***INF] Debug output is on"
fi

if [ $(expr $HOUR % 2) -eq 1 ]; then
	DATABASE=$DATABASE_LOCAL
else
	DATABASE=$DATABASE_REMOTE
fi

echo "[***INF] Database configuration is ${DATABASE}"

RAKULIB="${PHEIXLIB},${ETHELIALIB},${DOSSIERLIB}" raku ${WORKDIR}/bin/ethelia-stations.raku \
	--token=9e2f408f-****-****-****-a961e8ea8ae0 \
	--remotetable=${DATABASE} \
	--localstoragepath=$HOME/nvme/home/apache_root/pheix.org/www/conf/extensions/ \
	--remoteconfigpath=$HOME/nvme/home/apache_root/pheix.org/www/conf/addons/ExtensionEthelia/ \
	--unlock=False \
	--debug=${DEBUG} \
	--purge
