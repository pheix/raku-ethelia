#!/bin/bash

ME=`basename "$0"`
CONTAINER=ethelia-tg-bot
LOGDIR=$HOME
WORKDIR=$HOME/git/raku-ethelia
PHEIXLIB=$HOME/nvme/home/apache_root/pheix.org/git/dcms-raku/lib/
ETHELIALIB=$HOME/git/raku-ethelia/lib/
DOSSIERLIB=$HOME/git/pheix-research/raku-dossier-prototype/
BACKUPDIR=$HOME/webtech/logs/ethelia
LOGFILE=$BACKUPDIR/update-db/update-dbs.bash_`date +"%Y%m%d_%H-%M-%S"`.log
SIGNLOGGLOBPATTERN="ethelia-stations.raku-sgn-*"
DATABASES=("holesky/ExtensionEthelia" "local/holesky/ExtensionEthelia" "local/reserve/1/holesky/ExtensionEthelia" "local/reserve/2/holesky/ExtensionEthelia")
DEBUG="False"
MAXSTORAGES=3

# procedures
log_message () {
	echo -e "$1\n" | xargs -I{} logger --no-act -s "{}" 2>&1 | sed 's/^<[0-9]\+>//' >> ${LOGFILE}
}

sync_database () {
	local LOCALSTORAGEPATH=${HOME}/nvme/home/apache_root/pheix.org/www/conf/extensions/
	local REMOTECONFIGPATH=${HOME}/nvme/home/apache_root/pheix.org/www/conf/addons/ExtensionEthelia/
	local INDEX=$(expr `date +"%H"` % $MAXSTORAGES)
	local REMOTETABLE=${DATABASES[$INDEX]}

	log_message "[${ME} - INFO] database configuration is ${REMOTETABLE}"

	RAKULIB="${PHEIXLIB},${ETHELIALIB},${DOSSIERLIB}" raku ${WORKDIR}/bin/ethelia-stations.raku \
		--token=${AQICNTOKEN} \
		--remotetable=${REMOTETABLE} \
		--localstoragepath=${LOCALSTORAGEPATH} \
		--remoteconfigpath=${REMOTECONFIGPATH} \
		--unlock=False \
		--debug=${DEBUG} \
		--blobbing=True \
		--annotate=True \
		--create 2>&1 | \
			xargs -I{} logger --no-act -s "{}" 2>&1 | sed 's/^<[0-9]\+>//' >> "${LOGFILE}"
}

sync_logs () {
	if [ ${DEBUG} == 'False' ]; then
		for FILE in ${LOGDIR}/${SIGNLOGGLOBPATTERN}; do
			if [ -f ${FILE} ]; then
				docker cp ${FILE} ${CONTAINER}:/tg-bot/app/`basename "${FILE}"` 2>&1 | \
					xargs -I{} logger --no-act -s "{}" 2>&1 | sed 's/^<[0-9]\+>//' >> "${LOGFILE}"
				cp -rf ${FILE} ${BACKUPDIR} && rm -f ${FILE}
			fi;
		done

		docker cp ${LOGFILE} ${CONTAINER}:/tg-bot/app/`basename "${LOGFILE}"` 2>&1 | \
			xargs -I{} logger --no-act -s "{}" 2>&1 | sed 's/^<[0-9]\+>//' >> "${LOGFILE}"
	fi
}

# main body
while getopts "dm:" opt
do
	case $opt in
		d)
			DEBUG="True"
			LOGFILE=/dev/stdout

			log_message "[${ME} - INFO] debug output is on"
		;;
		m)
			NUMREGEXPR='^[0-9]+$'

			if [[ ${OPTARG} =~ $NUMREGEXPR ]]; then
				MAXSTORAGES=$(expr ${OPTARG} + 0)
			fi
		;;
		*)
			log_message "[${ME} - ERROR] usage: $ME -d"

			exit 0
		;;
	esac
done

log_message "[${ME} - INFO] logging to ${LOGDIR}"
log_message "[${ME} - INFO] processing max ${MAXSTORAGES} storages"

sync_database
sync_logs
