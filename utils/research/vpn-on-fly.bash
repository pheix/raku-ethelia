#!/bin/bash

ME="${0}";
STARTTIMEOUT=5
VPNHOST=    #192.168.10.30
VPNCFG=     #$HOME/.ovpn/southern-deadend.ovpn
VPNPID=

while getopts "c:h:" opt
do
    case $opt in
    c)
        VPNCFG=${OPTARG}
    ;;
    h)
        REGEXPR='^[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+$'

        if [[ ${OPTARG} =~ $REGEXPR ]] ; then
            VPNHOST=${OPTARG}
        else
            echo "[${ME} - ERROR] please give the corrent IP address to be checked"
            exit 1
        fi
    ;;
    *)
        # echo "No reasonable options found!"
    ;;
    esac
done

# check if IP address is given
if [ -z "${VPNHOST}" ]; then
    echo "[${ME} - ERROR] IP address is blank"
    exit 1
fi

# check if connection has been already established
if ping -c 1 ${VPNHOST} > /dev/null
then
    echo "[${ME} - INFO]: ${VPNHOST} is available"
    exit 0;
fi

# check if script is run by root
if [ $(id -u) -ne 0 ]; then
    echo "[${ME} - ERROR] please run this script as root or using sudo"
    exit 2
fi

# check if VPN configuration file is available
if [ -z ${VPNCFG} ] || [ ! -e ${VPNCFG} ] || [ ! -f ${VPNCFG} ]; then
    echo "[${ME} - ERROR] no VPN config <${VPNCFG}> is available"
    exit 3
fi;

bash -c "openvpn --auth-nocache --config ${VPNCFG}" > /dev/null 2>&1 & VPNPID=$!

echo "[${ME} - INFO] started VPN runner process ${VPNPID}"

sleep $STARTTIMEOUT

# check if connection is established
if ping -c 1 ${VPNHOST} > /dev/null
then
	echo "[${ME} - INFO] ${VPNHOST} is available"

    NODESIGNATURE=`curl -s -k https://${VPNHOST}:40881 -X POST -H "Content-Type: application/json" -d '{"jsonrpc": "2.0", "method": "web3_clientVersion", "params": [], "id": 1}' | jq -r .result`

    if [ ! -z "${NODESIGNATURE}" ]; then
        echo "[${ME} - INFO] ethereum node signature ${NODESIGNATURE}"
    else
        echo "[${ME} - WARNING] no ethereum node is available"
    fi

    kill -SIGTERM ${VPNPID}
else
    echo "[${ME} - ERROR] ${VPNHOST} is not reachable, check VPN runner process ${VPNPID} manually"
    exit 4
fi

# check if connection closed
if ping -c 1 ${VPNHOST} > /dev/null
then
    echo "[${ME} - ERROR] can not close connection to ${VPNHOST}, check VPN runner process ${VPNPID} manually"
    exit 5
else
    echo "[${ME} - INFO] connection to ${VPNHOST} is successfully closed"
fi

exit 0;
