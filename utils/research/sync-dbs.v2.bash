#!/bin/bash

ME=`basename "$0"`
LOGDIR=$HOME
DISTR=$HOME
CONTAINER=ethelia-tg-bot
NUMREGEXPR='^[0-9]+$'
SIGNLOGGLOBPATTERN="ethelia-stations.raku-sgn-*"
DEBUG="False"
DATABASES=("holesky/ExtensionEthelia" "local/holesky/ExtensionEthelia" "local/reserve/1/holesky/ExtensionEthelia" "local/reserve/2/holesky/ExtensionEthelia")
VPNFLAGS=(0 0 1 1)
SKIPINDEXES=()
STARTTIMEOUT=30
INACTIVITYTIMEOUT=600
MAXSTORAGES=3
MAXITERATIONS=5
PHEIXLIB=
ETHELIALIB=
DOSSIERLIB=
BACKUPDIR=
VPNHOST=
VPNCFG=
VPNPID=
INDEX=
VPNFLAG=

# procedures
log_message () {
	echo -e "$1\n" | xargs -I{} logger --no-act -s "{}" 2>&1 | sed 's/^<[0-9]\+>//' >> ${LOGFILE}
}

sync_database () {
	local REMOTETABLE=${DATABASES[$INDEX]}
	local LOCALSTORAGEPATH=${DISTR}/nvme/home/apache_root/pheix.org/www/conf/extensions/
	local REMOTECONFIGPATH=${DISTR}/nvme/home/apache_root/pheix.org/www/conf/addons/ExtensionEthelia/
	local ABIPATH=${DISTR}/git/dcms-raku/conf/system/eth/

	log_message "[${ME} - INFO] table configuration is ${REMOTETABLE}"
	log_message "[${ME} - INFO] local storage path ${LOCALSTORAGEPATH}"
	log_message "[${ME} - INFO] remote storage configuration path ${REMOTECONFIGPATH}"

	RAKULIB="${PHEIXLIB},${ETHELIALIB},${DOSSIERLIB}" raku ${WORKDIR}/bin/ethelia-stations.raku \
		--token=${AQICNTOKEN} \
		--remotetable=${REMOTETABLE} \
		--localstoragepath=${LOCALSTORAGEPATH} \
		--remoteconfigpath=${REMOTECONFIGPATH} \
		--abi=${ABIPATH} \
		--unlock=False \
		--debug=${DEBUG} \
		--blobbing=True \
		--annotate=True \
		--create 2>&1 | \
			xargs -I{} logger --no-act -s "{}" 2>&1 | sed 's/^<[0-9]\+>//' >> "${LOGFILE}"
}

sync_logs () {
	if [ ${DEBUG} == 'False' ]; then
		for FILE in ${LOGDIR}/${SIGNLOGGLOBPATTERN}; do
			if [ -f ${FILE} ]; then
				docker cp ${FILE} ${CONTAINER}:/tg-bot/app/`basename "${FILE}"` 2>&1 | \
					xargs -I{} logger --no-act -s "{}" 2>&1 | sed 's/^<[0-9]\+>//' >> "${LOGFILE}"
				cp -rf ${FILE} ${BACKUPDIR} && rm -f ${FILE}
			fi;
		done

		docker cp ${LOGFILE} ${CONTAINER}:/tg-bot/app/`basename "${LOGFILE}"` 2>&1 | \
			xargs -I{} logger --no-act -s "{}" 2>&1 | sed 's/^<[0-9]\+>//' >> "${LOGFILE}"
	fi
}

run_vpn () {
	# check if IP address is given
	if [ -z "${VPNHOST}" ]; then
		log_message "[${ME} - ERROR] IP address is blank"
		return 1
	fi

	# check if connection has been already established
	if ping -c 1 ${VPNHOST} > /dev/null
	then
		log_message "[${ME} - INFO]: ${VPNHOST} is already available, we are syncing"

		sync_database
		sync_logs

		return 0
	fi

	# check if script is run by superuser
	if [ $(id -u) -ne 0 ]; then
		log_message "[${ME} - ERROR] please run this script as root or using sudo"
	    return 2
	fi

	# check if VPN configuration file is available
	if [ -z ${VPNCFG} ] || [ ! -e ${VPNCFG} ] || [ ! -f ${VPNCFG} ]; then
		log_message "[${ME} - ERROR] no VPN config <${VPNCFG}> is available"
		return 3
	fi

	for ITERATION in $(seq 1 ${MAXITERATIONS})
	do
		bash -c "openvpn --inactive ${INACTIVITYTIMEOUT} --auth-nocache --config ${VPNCFG}" > /dev/null 2>&1 & VPNPID=$!

		log_message "[${ME} - INFO] started VPN runner process ${VPNPID} at iteration ${ITERATION}"

		sleep $STARTTIMEOUT

		# check if connection is established
		if ping -c 1 ${VPNHOST} > /dev/null
		then
			log_message "[${ME} - INFO] ${VPNHOST} is available, we are syncing at iteration ${ITERATION}"

			sync_database
			sync_logs

			kill -SIGTERM ${VPNPID}
			break
		else
			log_message "[${ME} - ERROR] ${VPNHOST} is not reachable, forcing to kill ${VPNPID} automatically at iteration ${ITERATION}"
			kill -SIGTERM ${VPNPID}

			if [ $ITERATION -eq $MAXITERATIONS ]
			then
				log_message "[${ME} - ERROR] can not establish connection to ${VPNHOST} in ${ITERATION} iterations"
				return 4
			fi
		fi
	done

	# check if connection closed
	if ping -c 1 ${VPNHOST} > /dev/null
	then
		log_message "[${ME} - ERROR] can not close connection to ${VPNHOST}, check VPN runner process ${VPNPID} manually"
		return 5
	else
		log_message "[${ME} - INFO] connection to ${VPNHOST} is successfully closed"
	fi

	return 0
}

# main body
while getopts "dc:h:p:m:s:i:" opt
do
	case $opt in
		d)
			DEBUG="True"
			LOGFILE=/dev/stdout

			log_message "[${ME} - INFO] debug output is on"
		;;
		c)
		    VPNCFG=${OPTARG}
		;;
		h)
			REGEXPR='^[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+$'

			if [[ ${OPTARG} =~ $REGEXPR ]] ; then
				VPNHOST=${OPTARG}
			else
				log_message "[${ME} - ERROR] please give the corrent IP address to be checked"

				exit 1
			fi
		;;
		p)
			if [ ! -z ${OPTARG} ] && [ -e ${OPTARG} ] && [ -d ${OPTARG} ]; then
				DISTR=$OPTARG
			fi
		;;
		m)
			if [[ ${OPTARG} =~ $NUMREGEXPR ]]; then
				MAXSTORAGES=$(expr ${OPTARG} + 0)
			fi
		;;
		s)
			SKIPINDEXREGEXPR='^[0-9,]+$'

			if [[ ${OPTARG} =~ $SKIPINDEXREGEXPR ]]; then
				_IFS=${IFS}
				IFS="${IFS},"

				read -a SKIPINDEXES <<< "${OPTARG}"
				log_message "[${ME} - INFO] skip processing nodes at indexes ${SKIPINDEXES[*]}"

				IFS=${_IFS}
			fi
		;;
		i)
			if [[ ${OPTARG} =~ $NUMREGEXPR ]]; then
				INDEX=$(expr ${OPTARG} + 0)
			fi
		;;
		*)
			log_message "Usage: $ME -d -c </path/to/config.ovpn> -h 127.0.0.1"

			exit 0
		;;
	esac
done

if [ -z "${INDEX}" ]; then
	INDEX=$(expr `date +"%H"` % $MAXSTORAGES)
else
	if [ $INDEX -ge $MAXSTORAGES ]; then
		log_message "[${ME} - ERROR] node index [$INDEX] is out of storage range $MAXSTORAGES"
		sync_logs

		exit 3
	fi
fi

for SKIPINDEX in ${SKIPINDEXES[@]}; do
	if [ ! -z "${SKIPINDEX}" ]; then
		if [ $SKIPINDEX -ge $MAXSTORAGES ] || [ $SKIPINDEX -eq $INDEX ]; then
			log_message "[${ME} - WARN] force skip for node index [$SKIPINDEX] processing"
			sync_logs

			exit 2
		fi
	fi
done

VPNFLAG=${VPNFLAGS[$INDEX]}
PHEIXLIB=${DISTR}/nvme/home/apache_root/pheix.org/git/dcms-raku/lib/
ETHELIALIB=${DISTR}/git/raku-ethelia/lib/
DOSSIERLIB=${DISTR}/git/pheix-research/raku-dossier-prototype/
BACKUPDIR=${DISTR}/webtech/logs/ethelia
WORKDIR=${DISTR}/git/raku-ethelia

if [ ${DEBUG} == 'False' ]; then
	LOGFILE=${BACKUPDIR}/update-db/update-dbs.bash_`date +"%Y%m%d_%H-%M-%S"`.log
fi

log_message "[${ME} - INFO] logging to ${LOGDIR}"
log_message "[${ME} - INFO] use ${DISTR} as home for distributive target"

if [ $VPNFLAG -eq 1 ]; then
	log_message "[${ME} - WARN] opening VPN tunnel"

	run_vpn
	RETVALUE=$?

	sync_logs

	exit $RETVALUE
else
	log_message "[${ME} - INFO] regular run w/o VPN tunnel"

	sync_database
	sync_logs
fi

exit 0
