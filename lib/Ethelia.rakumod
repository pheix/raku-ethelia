unit class Ethelia;

use Ethelia::Component;
use Ethelia::Explorer;

use MIME::Base64;
use JSON::Fast;

use Pheix::Addons::Embedded::Admin;
use Pheix::Controller::API;
use Pheix::Datepack;
use Pheix::Model::JSON;
use Pheix::Model::Database::Access;
use Pheix::View::Pages;
use Pheix::View::Web::Cookie;
use Pheix::Utils;

use Dossier::TelegramBot;
use Dossier::Transport::Request;
use Crypt::LibGcrypt::Random;
use HTML::EscapeUtils;

has      @!fields   = <id address code created payload topic>;
has Str  $!name     = 'ExtensionEthelia';
has Str  $.pheixabi = 'conf/system/eth/PheixDatabase.abi';
has Bool $.nocache  = False;
has Bool $.test     = False;
has Bool $.withcomp = True;
has UInt $.vperiod  = 90;
has Str  $.confpath;
has      $!ctrl;

has Pheix::Addons::Embedded::Admin $!adm = Pheix::Addons::Embedded::Admin.new;
has Ethelia::Component     $!component   = Ethelia::Component.new;
has Pheix::Controller::API $!builtinapi  = Pheix::Controller::API.new;
has MIME::Base64           $!mimeobj     = MIME::Base64.new;
has Pheix::Model::JSON     $.jsonobj     = self.init_json_config(:path($!confpath));

method init_json_config(Str :$path, Bool :$skipexternal = False) returns Pheix::Model::JSON {
    X::AdHoc.new(:payload(sprintf("no config file for %s addon found", $!name))).throw
        unless $path && $path.IO.e;

    my $json = Pheix::Model::JSON.new(:addonpath($path)).set_entire_config(:addon($!name));
    my $ecfg = $json.get_setting($!name, 'external-config-storage', 'value');

    return $json unless $ecfg && !$skipexternal;

    my $database = Pheix::Model::Database::Access.new(:table($ecfg), :fields([]), :jsonobj($json));

    X::AdHoc.new(:payload(sprintf("no database for external config in %s", $ecfg))).throw
        unless $database && $database.exists;

    my $config = Pheix::View::Pages
        .new(:utilobj(Pheix::Utils.new(:jsonobj($json))), :jsonobj($json))
        .raw_pg(:table($ecfg), :database($database));

    my $externalsetup = from-json($config && $config ne q{} ?? $config !! '{}');

    X::AdHoc.new(:payload(sprintf("no external config in %s", $ecfg))).throw
        unless $externalsetup && $externalsetup.keys;

    return Pheix::Model::JSON.new.set_entire_config(:addon($!name), :setup(%$externalsetup))
}

method get(:$ctrl!) returns Ethelia {
    $!ctrl = $ctrl.clone;

    return self;
}

method get_class returns Str {
    return self.^name;
}

method get_name returns Str {
    return $!name;
}

method get_sm returns List {
    return List.new
}

method error(Hash :$args, Str :$msg) returns Hash {
    return $!builtinapi.error(
        :route($args<match><details><path>),
        :code($args<code> // '404'),
        :message($msg // q{}),
        :tick($args<tick>),
        :sharedobj($args<shared>),
    );
}

method enrich_payload(Str :$token!, Hash :$payload!, Hash :$data) returns Hash {
    my $pcopy = $payload;

    my %directaccess =
        $!jsonobj.get_all_settings_for_group_member($!name, 'accesstokens', $token) //
            Hash.new;

    if %directaccess.keys.elems {
        $pcopy<enrichment> = %directaccess<enrichment>
            if %directaccess<enrichment> && %directaccess<enrichment>.keys;
    }

    if $data && $data.keys {
        for $data.kv -> $key, $value {
            next if $pcopy<enrichment>{$key};

            $pcopy<enrichment>{$key} = $value;
        }
    }

    return $pcopy;
}

method get_event_list(Str :$table = $!name, Bool :$latest = False, Bool :$withid = False) returns List {
    my @events;

    my $database = Pheix::Model::Database::Access.new(
        :table($table),
        :fields(List.new),
        :jsonobj($!jsonobj),
        :test($!test)
    );

    X::AdHoc.new(:payload('no database for event list')).throw unless $database;

    my @rows = $database.get_all(:fast(True), :withcomp($!withcomp));

    for @rows.values -> $row {
        next unless $row ~~ Hash && $row<data>;

        my $data = $database.dbswitch == 1 ?? $row<data> !!
            $!mimeobj.decode-str($row<data>);

        next unless $data && $data ~~ /<[\|]>+/;

        my @records = $data.split(q{|}, :skip-empty);

        if $database.dbswitch == 1 {
            @records.shift;
            @records.pop;
        }
        elsif $database.dbswitch == 0 && $withid {
            @records.unshift($row<id>);
        }

        my $update_existed_record = False;

        if $latest {
            for @events.kv -> $index, $event {
                my $addrpos = $withid ?? 1 !! 0;

                if $event[$addrpos] eq @records[$addrpos] {
                    @events[$index] = @records;

                    $update_existed_record = True;

                    last;
                }
            }
        }

        @events.push(@records) if @records && @records.values && !$update_existed_record;
    }

    return @events;
}

method validate_on_blockchain(Str :$token!, Str :$route!, Hash :$payload) returns Hash {
    my $a = $!ctrl.addons{$!adm.get_class}<objct>;

    return { error => sprintf("no admin class %s %s", $!adm.get_class, $!ctrl.addons{$!adm.get_class}.keys.gist) } unless $a && $a.^find_method('get').defined;

    my %v = $a.get(:ctrl($!ctrl)).get_authnode.validate_on_blockchain(:$token, :update(False));

    if !((%v<status>:exists) && %v<status>) {
        my %directaccess = $!jsonobj.get_all_settings_for_group_member($!name, 'accesstokens', $token) // Hash.new;

        if %directaccess.keys.elems {
            %v = status => False, expired => False;

            return %v unless %directaccess<active>;

            if %directaccess<routes> && %directaccess<routes>.elems {
                for %directaccess<routes>.values -> $allowedroute {
                    if $allowedroute eq $route {
                        %v<status> = True;

                        last;
                    }
                }
            }

            if %v<status> && %directaccess<created> && %directaccess<expired> {
                my $created_timestamp = DateTime.new(%directaccess<created>, :timezone($*TZ)).Instant.UInt;
                my $expiration        = Pheix::View::Web::Cookie.new.expire_calc(%directaccess<expired>);

                if $created_timestamp &&
                   $created_timestamp > 0 &&
                   $expiration &&
                   $expiration > 0 &&
                   $created_timestamp + $expiration > time
                {
                       %v<tx>      = $token;
                       %v<pkey>    = $!ctrl.sharedobj<utilobj>.get_token_by_now;
                       %v<session> = ($created_timestamp + $expiration) - time;
                       %v<addr>    = %directaccess<address>;
                       %v<scope>   = %directaccess<scope> // 'address';
                }
                else {
                    %v<status> = False;
                }
            }

            if %v<status> && %directaccess<automation> && %directaccess<automation>.keys {
                if %directaccess<automation><class> && %directaccess<automation><method> {
                    try {
                        %v<status> = ::(%directaccess<automation><class>)."%directaccess<automation><method>"(:$payload);

                        CATCH {
                            default {
                                my $e = .message;

                                X::AdHoc.new(:payload(sprintf("direct access automation is failed: %s", $e))).throw;
                            }
                        }
                    }
                }
            }
        }
    }

    return %v;
}

method extention_api_content(:%match!) returns Hash {
    my $comp_id   = sprintf("%s-%d", $!name.lc, time);
    my $container = $!component.build;
    my $table     = %match.keys && %match<storage> && %match<storage>.Str ~~ m/<[0..9 a..z A..Z _]>+/ ?? %match<storage>.Str !! $!name;

    "/tmp/dump.log".IO.spurt(sprintf("%s\n%s", %match.gist, $table));

    my @events = %match<details><validate><scope> && %match<details><validate><scope> eq 'full' ??
        self.get_event_list(:$table).reverse !!
            self.get_event_list(:$table).reverse.grep({ %match<details><validate><addr>.lc eq $_.head.lc });

    my $tparams = {
        id      => $comp_id,
        header  => $!name,
        content => $!ctrl.sharedobj<rsrcobj>.vocabulary<usrcnterror>,
        events  => []
    };

    if @events {
        $container = $!component.build(:key('regular'));

        my @e;

        @events.values.map({
            my %evhash;

            for $_.kv -> $key,$value {
                %evhash{@!fields[$key + 1]} = $value;
            }

            push(@e, %evhash);
        });

        $tparams = {
            id     => $comp_id,
            header => $!name,
            events => @e,
            component_name => $!component.get_component_name,
            component      => $!component.build(:key('search')),
            #component      => $!component.build,
        };
    }

    return {
        $!name.lc => {
            id        => $comp_id,
            name      => $!name,
            component => $!ctrl.sharedobj<mkupobj>.uni_tag($!name.lc, $container),
            tparams   => $tparams
        }
    }
}

method event_store_api(
         :%match!,
    Str  :$route,
    UInt :$tick!,
    Hash :$sharedobj!,
    Hash :$credentials = {},
    Hash :$payload = {},
    Hash :$header = {},
) returns Hash {
    my %rc;
    my %data;
    my $notification;
    my @chaindetails;

    my $table = %match.keys && %match<storage> && %match<storage>.Str ~~ m/<[0..9 a..z A..Z _]>+/ ?? %match<storage>.Str !! $!name;
    my $error = {
        match  => %match,
        tick   => $tick,
        shared => $sharedobj,
        code   => '400'
    };

    return self.error(:args($error), :msg('blank payload')) unless $payload && $payload.keys;

    return self.error(:args($error), :msg('corrupted payload')) unless $payload<code> && $payload<topic> && $payload<payload>;

    return self.error(:args($error), :msg('no access token'))
        unless $credentials<token> && $credentials<token> ~~ m:i/^ 0x<xdigit>**64 $/;

    my %validate = self.validate_on_blockchain(:token($credentials<token>), :$route, :payload($payload<payload>));

    if !((%validate<status>:exists) && %validate<status>) {
        $error<code> = '401';

        return self.error(:args($error), :msg(%validate<error> // 'access denied'));
    }

    for self.get_tables(:t($table)) -> $t {
        my $database = Pheix::Model::Database::Access.new(
            :table($t),
            :fields(List.new),
            :jsonobj($!jsonobj),
            :test($!test)
        );

        return self.error(:args($error), :msg(sprintf("Database for table %s failed", $t))) unless $database;

        return self.error(:args($error), :msg(sprintf("No table %s found", $database.table))) unless $database.exists;

        @!fields.map({ %data{$_} = $payload{$_} if $_ ne 'id'});

        %data<transaction>:delete;
        %data<created> =
            Pheix::Datepack.new(:date(DateTime.new(now)), :unixtime(time)).get_http_response_date;

        %data<topic>  ~~ s:g:i/<-[0..9a..z\-\_]>+//;
        %data<id>      = $database.get_count({}) + 1;
        %data<address> = %validate<addr>;

        my $blockchain_db = @chaindetails.grep({ $_<transactionHash>:exists }).head // {};

        %data<payload> = $!mimeobj.encode-str(
            %data<payload> ~~ Str || %data<payload> ~~ Numeric ??
                ~%data<payload> !! to-json(self.enrich_payload(:token($credentials<token>), :payload(%data<payload>), :data($blockchain_db)), :!pretty), :oneline);

        if $database.dbswitch == 1 {
            $database.chainobj.fields = @!fields;
            my $insert_params = {%data, waittx => False, compression => True};

            if !$database.insert($insert_params) {
                return self.error(:args($error) :msg(sprintf("Insert to blockchain table %s is failed", $database.table)));
            }
            else {
                if $insert_params<txhash> && $insert_params<txhash> ~~ m:i/^ 0x<xdigit>**64 $/ {
                    %data<transaction> = $insert_params<txhash>;
                }
            }
        }
        elsif $database.dbswitch == 0 {
            my $event = self.get_event_list(:table($database.table), :withid(True)).grep({
                $_.[1]  eq %data<address> &&
                $_.[2]  eq %data<code>    &&
                $_.tail eq %data<topic>
            }).tail;

            my $rec = %data.keys.sort.map({ %data{$_} if $_ ne 'id' }).join(q{|});

            if $event && $event.elems {
                %data<id> = $event.head.UInt;

                my $set_params = { data => $!mimeobj.encode-str($rec, :oneline) };

                if !$database.set($set_params, {id => %data<id>}).Bool {
                    return self.error(:args($error) :msg(sprintf("Set to filechain table %s is failed", $database.table)));
                }
            }
            else {
                my $insert_params = {
                    id => %data<id>,
                    data => $!mimeobj.encode-str($rec, :oneline),
                    compression => q{0}
                };

                if !$database.insert($insert_params) {
                    return self.error(:args($error) :msg(sprintf("Insert to filechain table %s is failed", $database.table)));
                }
            }
        }
        else {
            X::AdHoc.new(:payload(sprintf("wrong storage type %d for %s table", $database.dbswitch, $table))).throw;
        }

        if !$notification {
            my $notification_decoded = $!mimeobj.decode-str(%data<payload>);

            try {
                $notification = from-json($notification_decoded);

                CATCH {
                    default {
                        $notification = { error => .message, payload => $notification_decoded };
                    }
                }
            }
        }

        @chaindetails.push({
            table => $database.table,
            id    => %data<id>,
            %data<transaction> ?? %(transactionHash => %data<transaction>) !! %()
        });
    }

    return {
        component => $sharedobj<mb64obj>.encode-str($sharedobj<mkupobj>.uni_tag('div',
            $sharedobj<mkupobj>.uni_tag('div', $sharedobj<rsrcobj>.vocabulary<storeok>, :attrs({
                class => 'alert alert-success _phx-cntr',
                role  => 'alert'
            }))), :oneline
        ),
        tparams => { tmpl_sesstoken => $sharedobj<pageobj>.get_tparams<tmpl_sesstoken>, },
        event => %data<topic>,
        chaindetails => @chaindetails,
        result => 'success',
        notification => self.send_notification(
            :topic(%data<topic>),
            :payload($notification)
        )
    };
}

method event_search_api(
    :%match!,
    Str  :$route,
    UInt :$tick!,
    Hash :$sharedobj!,
    Hash :$credentials = {},
    Hash :$payload = {},
    Hash :$header = {},
) returns Hash {
    my $start = now;
    my $table = %match.keys && %match<storage> && ~%match<storage>.Str ~~ m/<[0..9 a..z A..Z _]>+/ ?? %match<storage>.Str !! $!name;
    my $error = {
        match  => %match,
        tick   => $tick,
        shared => $sharedobj,
        code   => '401'
    };

    my $a  = $!ctrl.addons{$!adm.get_class}<objct>;
    my %rc = tparams => { events => [] };

    return %rc unless $payload && $payload.keys && $payload<search> && $payload<search>.keys;

    return %rc unless $payload<search><value> ne q{} && ($payload<search><target> ~~ 1 .. 2);

    X::AdHoc.new(:payload(sprintf("No admin object found by class %s", $!adm.get_class))).throw
        unless $a && $a.^find_method('get').defined;

    my Str $token = Pheix::View::Web::Cookie.new.get_value(:name('pheixauth'), :cookies($sharedobj<fastcgi>.env<HTTP_COOKIE> // q{}));

    $token = $credentials<token> if $credentials<token> ~~ m:i/^ 0x<xdigit> ** 64 $/;

    if $token && $token ~~ m:i/^ 0x<xdigit>**64 $/ {
        my %blockchain = self.validate_on_blockchain(:token($token), :$route);

        if (%blockchain<status>:exists) && %blockchain<status> {
            my $latest = %blockchain<scope> && %blockchain<scope> eq 'latest' ?? True !! False;
            my @events = self.get_event_list(:$table, :$latest).map({
                $_ if (($payload<search><target> == 1 && $_[1] eq $payload<search><value>) ||
                      ($payload<search><target> == 2 && $_.tail eq $payload<search><value>) ||
                      $payload<search><value> eq q{*}) && (%blockchain<addr> eq $_.head || %blockchain<scope> ~~ /'full'|'latest'/)
            }).reverse;

            %rc<tparams><events>  = @events if (@events && @events.elems);
        }
        else {
            return self.error(:args($error) :msg('Token is not authorized on blockchain'));
        }
    }
    else {
        return self.error(:args($error) :msg(sprintf("Incorrect token <%s>", $token)));
    }

    %rc<component_render> = (now - $start).Str;
    %rc<header> = $header if $header && $header.keys.elems;

    return %rc;
}

method event_decode_api(
    :%match!,
    Str  :$route,
    UInt :$tick!,
    Hash :$sharedobj!,
    Hash :$credentials = {},
    Hash :$payload = {},
    Hash :$header = {},
) returns Hash {
    my $start = now;
    my $error = {
        match  => %match,
        tick   => $tick,
        shared => $sharedobj,
        code   => '401'
    };

    my $a  = $!ctrl.addons{$!adm.get_class}<objct>;
    my %rc = tparams => { transaction => {} };

    return %rc unless
        $payload &&
        $payload.keys &&
        $payload<inputdata> && $payload<inputdata> ~~ m:i/^ 0x<xdigit>+ $/;

    X::AdHoc.new(:payload(sprintf("No admin object found by class %s", $!adm.get_class))).throw
        unless $a && $a.^find_method('get').defined;

    my Str $token = $credentials<token>;

    if $token && $token ~~ m:i/^ 0x<xdigit>**64 $/ {
        my %blockchain = self.validate_on_blockchain(:token($token), :$route);

        if (%blockchain<status>:exists) && %blockchain<status> {
            my $decoded = {};

            try {
                $decoded = Ethelia::Explorer
                    .new(:abi($!pheixabi))
                    .decode(:function($payload<function>), :inputdata($payload<inputdata>));

                CATCH {
                    default {
                        $error<code> = '500';
                        my $msg      = .message;

                        return self.error(:args($error) :$msg);
                    }
                }
            }

            %rc<tparams><transaction> = $decoded if ($decoded && $decoded.keys);
        }
        else {
            return self.error(:args($error) :msg('Token is not authorized on blockchain'));
        }
    }
    else {
        return self.error(:args($error) :msg(sprintf("Incorrect token <%s>", $token)));
    }

    %rc<component_render> = (now - $start).Str;
    %rc<header> = $header if $header && $header.keys.elems;

    return %rc;
}

method send_notification(Str :$topic!, Hash :$payload!, Str :$key) returns Bool {
    return False unless $payload.keys;

    return False if $key && $key ne q{} && (!($payload{$key}:exists) || $payload{$key} eq q{});

    my Str $notifier = ~($!jsonobj.get_setting($!name, 'defaultnotifier', 'value'));

    return False unless $notifier && $notifier ne q{};

    my %notifierconfig =
        $!jsonobj.get_all_settings_for_group_member($!name, 'notifier', $notifier) //
            Hash.new;

    return False unless %notifierconfig.keys;

    return False unless %notifierconfig<notifyontopics>.grep($topic).elems;

    my $message = $key ?? $payload{$key} !! to-json($payload);

    if $notifier eq 'matrix' {
        return False unless
            (%notifierconfig<authendpoint>:exists) &&
            (%notifierconfig<notificationendpoint>:exists) &&
            (%notifierconfig<user>:exists) &&
            (%notifierconfig<password>:exists) &&
            (%notifierconfig<roomid>:exists) &&
            ((%notifierconfig<notifyontopics>:exists) && %notifierconfig<notifyontopics>.elems);

        return False unless %notifierconfig<notifyontopics>.grep($topic).elems;

        my $token_transport = Dossier::Transport::Request.new(:endpoint(%notifierconfig<authendpoint>));

        my $token_request = {
            type     => 'm.login.password',
            user     => %notifierconfig<user>,
            password => %notifierconfig<password>
        };

        my $token_response = $token_transport.send(:data($token_request));

        X::AdHoc.new(:payload(sprintf("no access token is returned in response: %s", $token_response.gist))).throw unless
            ($token_response<access_token>:exists) && $token_response<access_token> ne q{};

        my $notification_endpoint = sprintf("%s/%s/send/m.room.message/%s?access_token=%s",
            %notifierconfig<notificationendpoint>,
            %notifierconfig<roomid>,
            self!UUIDv4,
            $token_response<access_token>
        );

        my $notification_transport = Dossier::Transport::Request.new(:endpoint($notification_endpoint));

        my $notification_request = {
            msgtype => 'm.text',
            body => q{},
            format => 'org.matrix.custom.html',
            formatted_body => sprintf('<b><font color=red>%s</font><div><pre>%s</pre></div>', $topic, escape($message)),
        };

        my $notification_response = $notification_transport.send(:data($notification_request), :method('PUT'));

        X::AdHoc.new(:payload(sprintf("no event id is returned in response: %s", $notification_response.gist))).throw unless
            ($notification_response<event_id>:exists) && $notification_response<event_id> ne q{};

        return True;
    }
    elsif $notifier eq 'telegram' {
        return False unless
            (%notifierconfig<token>:exists) &&
            (%notifierconfig<chatid>:exists) &&
            ((%notifierconfig<notifyontopics>:exists) && %notifierconfig<notifyontopics>.elems);

        return False unless %notifierconfig<notifyontopics>.grep($topic).elems;

        my $api   = Dossier::TelegramBot.new(:api(Dossier::Transport::Request.new), :token(%notifierconfig<token>));
        my $ret   = False;
        $message ~~ s:g/\`/@/;

        try {
            $api.sendMessage(
                {
                    parse_mode => 'Markdown',
                    chat_id    => %notifierconfig<chatid>,
                    text       => sprintf("*%s*\n```event\n%s```", $topic, $message),
                }
            );

            $ret = True;
        }

        return $ret;
    }
    else {
        X::AdHoc.new(:payload(sprintf("wrong notifier %s: only Matrix and Telegram are supported", $notifier))).throw;
    }

    return False;
}

method get_tables(Str :$table = $!name) returns List {
    my %tabsets = $!jsonobj.get_all_settings_for_group_member($!name, 'storage', $table);

    return %tabsets && %tabsets.keys && %tabsets<link> && %tabsets<link>.Str ~~ m/<[0..9 a..z A..Z _ \/]>+/ ??
        [%tabsets<link>, $table] !!
            [$table];
}

method !UUIDv4 returns Str {
    my buf8 $buf = random(16);

    $buf[6] +|= 0b01000000;
    $buf[6] +&= 0b01001111;
    $buf[8] +|= 0b10000000;
    $buf[8] +&= 0b10111111;

    return (:256[$buf.values].fmt("%32.32x") ~~ /(........)(....)(....)(....)(............)/).join("-").lc;
}

method validate_captcha(Hash :$payload!) returns Bool {
    "/tmp/dump.txt".IO.spurt(sprintf("%s", to-json($payload)));
    return False unless $payload.keys;

    my $decrypted;
    my $public_code = $payload<public>;

    return False unless $public_code ~~ /^<:digit>+$/ && $payload<private>;

    try {
        $decrypted = Pheix::Utils.new(:$!jsonobj).do_decrypt($payload<private>);

        CATCH {
            default { return False; }
        }
    }

    return False unless $decrypted ~~ /^ <[\d\:]>+ $/;

    my ($code, $timestamp) = $decrypted.split(q{:}, :skip-empty);

    return False unless $code.match: /^ $public_code /;

    return False unless $timestamp > 0 && (time - $timestamp < $!vperiod);

    return True;
}
