unit class Ethelia::Component;

use Pheix::View::HTML::Markup;

has $!cnames = {
    search => 'extension-ethelia-search-component'
};

has      $!markup = Pheix::View::HTML::Markup.new;
has Str  $!js_pri = q:to/SCRIPT/;
export default {
    b64EncodeUnicode(str) {
        return btoa(encodeURIComponent(str).replace(/%([0-9A-F]{2})/g,
            function toSolidBytes(match, p1) {
                return String.fromCharCode('0x' + p1);
        }));
    },
    onBeforeMount() {
        var cname = this.props.component_name;

        try {
            riot.inject(riot.compileFromString(this.props.component).code, cname, './'+cname+'.riot');
            console.log(cname + ' is registered');
        }
        catch (e) {
            console.warn(cname + ' can not be registered');
        }

        if (this.props.events && this.props.events.length) {
            this.state = { events: this.props.events }
        }
        else {
            this.state = { events: [] }
        }
    },
    onMounted() {
        var cname = this.props.component_name;

        jQuery('#search-event-form').on("submit", { component:this }, this.doPost);
        console.log(this.props.id + '/' + cname + ' primary component is loaded');
    },
    doPost(event) {
        if (window.PheixAPI.isLoading) {
            window.PheixAuth.showNotification({text: 'Skip search due to request API in background'});
            console.info('skip search while requesting API in background');

            return;
        }

        var searchValue  = jQuery('#admin-event-search-query').val();
        var searchTarget = jQuery('#admin-event-search-target').val();

        if (!searchTarget || (searchTarget != 1 && searchTarget != 2)) {
            searchTarget = 1;
        }

        if (!searchValue) {
            console.error('no search value is given');

            return;
        }

        const query = searchValue.split(':');

        var request;
        var route = '/api/ethelia/event/search';

        if (query.length > 1) {
            route   = route.concat('/' + query[0]);
            request = query.slice(1).join(':');
        }
        else {
            request = query[0];
        }

        console.info('search route ' + route + ', request ' + request + ', target ' + (searchTarget == 1 ? ' code' : ' topic'));

        var searchRequest = {
            credentials: { token: '0x0' },
            method: 'GET',
            route: route,
            httpstat: '200',
            message: '',
            payload: {
                search: {
                    value: request,
                    target: searchTarget
                }
            }
        };

        console.log(searchRequest);

        var jqxhr = jQuery.post('/api', JSON.stringify(searchRequest, null, 2), function(response) {
            var response_obj;

            try { response_obj = JSON.parse(response); }
            catch(e) {
                console.error('JSON response parse failure');
                console.log(response);
            }

            if (response_obj && response_obj.content) {
                if (response_obj.content.tparams) {
                    if (response_obj.content.tparams.events && response_obj.content.tparams.events.length) {
                        event.data.component.update({ events: response_obj.content.tparams.events });
                    }
                    else {
                        var msg = 'No events are found';
                        window.PheixAuth.showDialog(event.data.component.b64EncodeUnicode(msg));
                    }
                }
                else {
                    console.warn('no tparams in response: ' + response_obj.content);
                }
            }
            else {
                console.error('no content in response');
            }
        })
        .done(function() {})
        .fail(function() {
            console.log('API request is failed for extension ' + event.data.component.props.component_name);
        })
        .always(function() {
            console.log('API request is finished for extension ' + event.data.component.props.component_name);
        });
    },
}
SCRIPT

has Str $!js_sec = q:to/SCRIPT/;
export default {
    b64DecodeUnicode(str) {
        return decodeURIComponent(atob(str).split('').map(function(c) {
            return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
        }).join(''));
    },
    b64CheckData(data) {
        var ret;

        try {
            this.b64DecodeUnicode(data);
            ret = true;
        }
        catch(e) {
            ret = false;
        }

        return ret;
    }
}
SCRIPT

has Hash $!componentmap = {
    default => {
        a-header => {
            tag   => 'h2',
            attrs => {
                class => 'h4 mb-3',
            },
            text  => '{props.header}'
        },
        b-content => {
            tag   => 'p',
            text  => '{props.content}'
        },
        c-script => {
            tag   => 'script',
            text  => ~$!js_sec
        }
    },
    regular => {
        a-header => {
            tag   => 'h2',
            attrs => {
                class => 'h4 mb-3'
            },
            text  => '{props.header}'
        },
        b-content-search => {
            tag   => 'div',
            attrs => {
                class => 'form-inline _pheix-admin-form-search mt-2 pb-3'
            },
            text  =>  {
                a-form => {
                    tag   => 'form',
                    attrs => {
                        id => 'search-event-form',
                        action => 'javascript:void(0);'
                    },
                    text => {
                        a-input  => {
                            tag   => 'input',
                            attrs => {
                                type        => 'text',
                                class       => 'form-control mr-2 _phx-transbg _pheix-admin-form-search-control',
                                placeholder => 'Search phrase',
                                required    => 1,
                                autofocus   => 1,
                                id          => 'admin-event-search-query'
                            },
                            text => q{}
                        },
                        b-select => {
                            tag   => 'select',
                            attrs => {
                                class => 'custom-select my-1 mr-sm-2 _phx-transbg _pheix-admin-form-search-control',
                                id    => 'admin-event-search-target',
                            },
                            text  => {
                                a-option => {
                                    tag   => 'option',
                                    attrs => {
                                        value    => 1,
                                        selected => 1
                                    },
                                    text => 'Code'
                                },
                                b-option => {
                                    tag   => 'option',
                                    attrs => {
                                        value => 2,
                                    },
                                    text => 'Topic'
                                }
                            }
                        },
                        c-button => {
                            tag   => 'input',
                            attrs => {
                                type    => 'submit',
                                class   => 'btn btn-primary my-1',
                                value   => 'Submit'
                            },
                            text => q{}
                        }
                    }
                }
            }
        },
        с-content-table => {
            tag   => 'div',
            attrs => {
                id => 'admin-event-search-results'
            },
            text => {
                a-component => {
                    tag   => $!cnames<search> // 'default-search-component',
                    attrs => {
                        name   => '{ props.component_name }',
                        id     => '{ props.id }',
                        events => '{ state.events }'
                    },
                    text  => q{}
                }
            }
        },
        d-script => {
            tag   => 'script',
            text  => ~$!js_pri
        }
    },
    search => {
        a-extension-container => {
            tag   => 'div',
            attrs => {
                class => '_pheix-admin-extension-container',
            },
            text  => {
                table => {
                    tag   => 'table',
                    attrs => {
                        #if    => 'props.events.length',
                        #class => 'w-100'
                    },
                    text  => {
                        tr => {
                            tag   => 'tr',
                            attrs => {
                                each => '{row in Object.values(props.events)}'
                            },
                            text  => {
                                td => {
                                    tag   => 'td',
                                    attrs => {
                                        each  => '{column in Object.values(row)}'
                                    },
                                    text  => {
                                        link => {
                                            tag   => 'a',
                                            attrs => {
                                                if    => '{b64CheckData(column)}',
                                                href  => 'javascript:window.PheixAuth.showDialog(\'{column}\');'
                                            },
                                            text  => '{column.substring(1, 52)+\'…\'}',
                                        },
                                        span => {
                                            tag   => 'span',
                                            attrs => {
                                                if => '{!b64CheckData(column) && !column.startsWith("0x")}'
                                            },
                                            text  => '{column}'
                                        },
                                        addr => {
                                            tag   => 'span',
                                            attrs => {
                                                if    => '{!b64CheckData(column) && column.startsWith("0x")}',
                                                class => '_phx-opacity-5',
                                                title => '{column}'

                                            },
                                            text  => '{column.substring(0, 8)+\'…\'}'
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        },
        b-script => {
            tag  => 'script',
            text => ~$!js_sec
        }
    }
};

method map_traverse(Hash :$node) returns Str {
    return unless $node && $node.keys;

    my Str $content;

    for $node.keys.sort -> $item {
        my $tag = $node{$item};

        next unless $tag<tag> && $tag<tag> ne q{};

        $content ~= $!markup.uni_tag(
            $tag<tag>,
            (
                $tag<text> ~~ Str ??
                    $tag<text> !!
                        self.map_traverse(:node($tag<text>))
            ),
            :attrs($tag<attrs> // {})
        );
    }

    return $content;
}

method build(Str :$key = 'default') returns Str {
    my $node = $!componentmap{$key};

    return q{} unless $node;

    return $!markup.uni_tag('div', self.map_traverse(:node($node)));
}

method get_component_name(Str :$key = 'search') returns Str {
    return $!cnames{$key} // q{};
}
