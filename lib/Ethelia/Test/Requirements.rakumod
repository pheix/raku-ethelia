unit class Ethelia::Test::Requirements;

has Str $.name;

use Pheix::Model::JSON;

method init_json_config(:%setup) returns Pheix::Model::JSON {
    return %setup.keys.elems ??
        Pheix::Model::JSON.new.set_entire_config(:addon($!name), :setup(%setup)) !!
            Pheix::Model::JSON.new(:addonpath('custom-config')).set_entire_config(:addon($!name));
}
