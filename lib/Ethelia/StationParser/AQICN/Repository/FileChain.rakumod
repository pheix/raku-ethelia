unit class Ethelia::StationParser::AQICN::Repository::FileChain;

use JSON::Fast;
use MIME::Base64;
use Pheix::Model::Database::Access;
use Pheix::Model::JSON;

has Str  $.table;
has Hash $.config;

has MIME::Base64                   $.base64 = MIME::Base64.new;
has Pheix::Model::Database::Access $.dbobj  = Pheix::Model::Database::Access.new(
    :table($!table),
    :fields(List.new),
    :jsonobj(Pheix::Model::JSON.new.set_entire_config(:setup($!config)))
);

method select_all returns List {
    return $!dbobj.get_all(:fast(True), :withcomp(True));
}

method max_id returns UInt {
    return self.select_all.map({$_<id>.Int}).sort.tail // 0;
}

method insert(:@fields!, Hash :$station!, Hash :$payload!) returns Bool {
    my $id = self.max_id + 1;

    (my $topic = $station<title> // $*DISTRO.gist) ~~ s:g/<:!alpha>//;

    my $record = {
        address => $station<addr>,
        code    => sprintf("aqicn-%s", $id),
        created => Pheix::Datepack.new(:date(DateTime.new(now)), :unixtime(time)).get_http_response_date,
        payload => $!base64.encode-str(to-json($payload), :oneline),
        topic   => $topic
    };

    my $insert_data = {
        id          => $id,
        data        => $!base64.encode-str(@fields.map({$record{$_}}).join(q{|}), :oneline),
        compression => q{0},
        waittx      => False,
    };

    return $!dbobj.insert($insert_data);
}

method set(UInt :$id!, :@fields!, :@columns!, Hash :$payload!) returns Bool {
    my $set_clause = { id => $id };

    my $record = {
        address => @columns.head,
        code    => @columns[1],
        created => @columns[2],
        payload => $!base64.encode-str(to-json($payload), :oneline),
        topic   => @columns.tail,
    };

    my $set_data = { data => $!base64.encode-str(@fields.map({$record{$_}}).join(q{|}), :oneline) };

    return $!dbobj.set($set_data, $set_clause, :waittx(False)).Bool;
}
