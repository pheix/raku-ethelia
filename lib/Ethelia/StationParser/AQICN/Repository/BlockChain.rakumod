unit class Ethelia::StationParser::AQICN::Repository::BlockChain;

use URI;
use MIME::Base64;
use Pheix::Datepack;
use Pheix::Model::Database::Access;
use Pheix::Model::Database::Blockchain;
use Pheix::Model::Database::Blockchain::SendTx;
use Pheix::Model::JSON;
use Pheix::Test::Blockchain;

has Str  $.table;
has Hash $.config;
has UInt $.retries = 5;
has UInt $.timeout = 5;
has Bool $.debug   = False;
has Bool $.test    = False;
has @.trxlog;
has $.diag is default(sub _diag(Str:$m){$m.say}) is rw;


has MIME::Base64                   $.base64 = MIME::Base64.new;
has Pheix::Model::Database::Access $.dbobj  = Pheix::Model::Database::Access.new(
    :table($!table),
    :fields(List.new),
    :jsonobj(Pheix::Model::JSON.new.set_entire_config(:setup($!config))),
    :debug($!debug),
);

method select_all(Bool :$withcomp = True, Bool :$withblob = False) returns List {
    my @database;

    X::AdHoc.new(:payload('select all on blobs is not supported yet')).throw if $withblob;

    my $co   = $!dbobj.chainobj;
    my $size = $co.count_rows;

    if $size > 0 {
        for ^$size -> $index {
            my Bool $comp = False;
            my UInt $id   = self.trycall({$co.get_id_byindex(:$index)}, :callframe(callframe));

            $comp =  self.trycall({$co.is_row_compressed(:$id) if $withcomp}, :callframe(callframe));

            if (my Str $data = self.trycall({$co.get_data_byindex(:$index, :$comp)}, :callframe(callframe))) {
                @database.push({
                    id          => $id,
                    data        => $data,
                    compression => $comp,
                });
            }
        }

        if $size != @database.elems {
            X::AdHoc.new(:payload(sprintf("select all from %s failed: need %d -> get %s rows", $!dbobj.table, $size, @database.elems))).throw
        }
        else {
            return @database;
        }
    }

    return List.new;
}

multi method store(UInt :$id!, :@fields!, :@columns!, Bool :$insert = True) returns Bool {
    my $data;
    my %result;
    my UInt $i = 0;

    my $co = $!dbobj.chainobj;

    $co.fields = @fields;
    $co.fields.map({ $data{$_} = @columns[$i++]; });

    if $insert {
        %result = self.trycall({$co.row_insert(:$id, :$data, :waittx(False), :comp(True))}, :callframe(callframe));
    }
    else {
        %result = self.trycall({$co.update(:$id, :$data, :waittx(False), :comp(True))}, :callframe(callframe));
    }

    return False unless %result<status>;

    my @trxlogdata = $co.sgnlog.tail.split(q{|}, :skip-empty);

    @!trxlog.push({
        id      => $id,
        txhash  => %result<txhash>,
        nonce   => @trxlogdata[1].UInt // 0,
        caller  => $insert ?? 'insert' !! 'set',
        created => Pheix::Datepack.new(:date(DateTime.new(now)), :unixtime(time)).get_http_response_date,
        type    => @trxlogdata.tail // q{},
    });

    return True;
}

multi method store(buf8 :$blob!, Bool :$annotate = False) returns Bool {
    my $co = $!dbobj.chainobj;

    X::AdHoc.new(:payload('empty blob')).throw unless $blob.bytes;

    X::AdHoc.new(:payload('can not continue without signer')).throw unless
        $co.sgnobj && $co.sgnobj ~~ Pheix::Model::Database::Blockchain;

    my $uri        = URI.new($co.apiurl);
    my $data       = $annotate ?? { cli => $co.ethobj.web3_clientVersion // sprintf("%s://%s:%d", $uri.scheme, $uri.host, $uri.port) } !! {};
    my Bool $ret   = $co.set_blob_data(:blobs([$blob]), :$data);
    my @trxlogdata = $co.sgnlog.tail.split(q{|}, :skip-empty);

    X::AdHoc.new(:payload('blob transaction failed')).throw unless
        $ret && @trxlogdata[4] ~~ m:i/^ 0x<xdigit>**64 $/;

    @!trxlog.push({
        id      => -1,
        txhash  => @trxlogdata[4],
        nonce   => @trxlogdata[1].UInt // 0,
        caller  => 'blob',
        created => Pheix::Datepack.new(:date(DateTime.new(now)), :unixtime(time)).get_http_response_date,
        type    => @trxlogdata.tail // q{},
    });

    return True;
}

method trycall(Block $call, CallFrame :$callframe) {
    my $e;
    my $b;

    for ^$!retries -> $try {
        try {
            return $call();

            CATCH {
                default {
                    $e = .message;
                    $b = .backtrace;

                    $!diag(:m(sprintf("[WARNING %s] error at %s on try %d (nonce=%d) while calling code: %s",
                        DateTime.now(formatter => {
                            sprintf(
                                "%04d%02d%02d-%02d:%02d:%02u.%04u",
                                .year,
                                .month,
                                .day,
                                .hour,
                                .minute,
                                .second.Int,
                                (.second % (.second.Int || .second)) * 10_000
                            )
                        }),
                        $callframe ?? sprintf("%s:%s", $callframe.file, $callframe.line) !! &?ROUTINE.name,
                        $try, $!dbobj.chainobj.nonce, $e
                    )));
                }
            }
        }

        sleep($!timeout);
    }

    X::AdHoc.new(:payload(sprintf("failed %d times while calling a code: %s\n%s", $!retries, $e, $b))).throw;
}
