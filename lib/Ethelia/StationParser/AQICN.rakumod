unit class Ethelia::StationParser::AQICN;

use MIME::Base64;
use JSON::Fast;

use Net::Ethereum;
use Node::Ethereum::Keccak256::Native;

use Ethelia::StationParser::AQICN::Repository::BlockChain;
use Ethelia::StationParser::AQICN::Repository::FileChain;
use Dossier::Transport::Request;
use Pheix::Datepack;
use Pheix::Model::JSON;
use Pheix::Datepack;
use Pheix::Model::Database::Access;

has Str  $.token;
has Str  $.endpoint = 'https://api.waqi.info/v2/map/bounds?latlng=42.682435,74.359589,43.032259,74.825821&networks=all';
has UInt $.retries  = 6;
has UInt $.timeout  = 5;
has Rat  $.trxdelay = 1.5;
has Bool $.blobbing = False;
has Bool $.annotate = False;
has Bool $.debug    = False;
has UInt $.expired_threshold = 4000;
has @.trxlog;

has Pheix::Model::Database::Access    $.dbobj;
has Node::Ethereum::Keccak256::Native $.keccak256   = Node::Ethereum::Keccak256::Native.new;
has Net::Ethereum                     $.netethereum = Net::Ethereum.new;
has MIME::Base64                      $.base64      = MIME::Base64.new;
has Pheix::Datepack                   $.datepack    = Pheix::Datepack.new;

has @!fields = <address code created payload topic>;
has Str $!used_endpoint = q{};

method get_used_endpoint returns Str {
    return $!used_endpoint;
}

method stations returns List {
    my $endpoint  = sprintf("%s&token=%s", $!endpoint, $!token);
    my $transport = Dossier::Transport::Request.new(:$endpoint);

    for ^$!retries {
        my %payload = $transport.send(:data({}), :method('GET'));

        if %payload<status> && %payload<status> eq 'ok' && %payload<data> && %payload<data>.elems {
            my @stations = %payload<data>.grep({ $_<aqi> ne '-' }).List;

            return @stations.map({
                %(
                    title => self.translit(:text($_<station><name>)),
                    sensorvalue => $_<aqi>,
                    profile => sprintf("https://aqicn.org/station/@%d/", $_<uid>.abs),
                    addr => $!netethereum.buf2hex($!keccak256.keccak256(:msg($_<station><name>))).lc.substr(0,42),
                    position => %(
                        lat => $_<lat>,
                        lng => $_<lon>,
                    ),
                )
            }).List;
        }
        else {
            X::AdHoc.new(:payload(sprintf("station fetch failure: %s", to-json(%payload)))).throw;
        }

        sleep($!timeout);
    }

    return List.new;
}

method synclocal(:@stations!, Str :$table, Str :$path) returns Bool {
    return False unless @stations.elems;

    my $repo;
    my @providers;

    if !$!dbobj {
        X::AdHoc.new(:payload(sprintf("table %s path %s is not found", $table, $path))).throw unless $table && $path.IO.d;

        $repo = Ethelia::StationParser::AQICN::Repository::FileChain.new(:$table, :config(self.config(:$table, :$path)));
    }
    else {
        $repo = Ethelia::StationParser::AQICN::Repository::FileChain.new(:$!dbobj);
    }

    X::AdHoc.new(:payload(sprintf("table %s is not existed", $repo.dbobj.table))).throw unless $repo.dbobj.exists;

    for @stations.values -> $station {
        my $existed = False;
        my $payload = {
            enrichment => {
                position => $station<position>,
                title => $station<title>,
                profile => $station<profile>
            },
            sensorvalue => $station<sensorvalue>,
            sensordevice => self.^name,
        };

        for $repo.select_all.values -> $record {
            my @columns;

            next unless $record ~~ Hash && $record<data>;

            try {
                @columns = $!base64.decode-str($record<data>).split(q{|}, :skip-empty);

                CATCH {
                    default { next; }
                }
            }

            next unless @columns.elems == @!fields.elems;

            if $station<addr> eq @columns.head {
                @columns[2] = Pheix::Datepack.new(:date(DateTime.new(now)), :unixtime(time)).get_http_response_date;

                return False unless $repo.set(:id($record<id>.UInt), :@!fields, :@columns, :$payload);

                $existed = True;

                last;
            }
        }

        if !$existed {
            return False unless $repo.insert(:@!fields, :$station, :$payload);
        }
    }

    return True;
}

method syncremote(:@localdb!, Str :$table, Str :$path, Hash :$overrideconfig) returns Bool {
    return False unless @localdb.elems;

    my $repo;

    if !$!dbobj {
        X::AdHoc.new(:payload(sprintf("table %s path %s is not found", $table, $path))).throw unless $table && $path.IO.d;

        my $config = self.config(:$table, :$path, :$overrideconfig, :build(False));
        $repo = Ethelia::StationParser::AQICN::Repository::BlockChain.new(:$table, :$config, :$!debug);
    }
    else {
        $repo = Ethelia::StationParser::AQICN::Repository::BlockChain.new(:$!dbobj);
    }

    if $repo.dbobj && $repo.dbobj.dbswitch == 1 {
        $repo.dbobj.chainobj.ethobj.keepalive = False;

        if $!blobbing {
            my @data;

            for @localdb.values -> $record {
                my @columns = self.check_local_record(:$record);

                next unless @columns && @columns.elems;

                @data.push(sprintf("%s|%s|0", $record<id>, $record<data>));
            }

            return False unless $repo.store(:blob(buf8.new(@data.join("\n").encode)), :$!annotate);
        }
        else {
            $repo.trycall({$repo.dbobj.chainobj.table_create(:waittx(True))}, :callframe(callframe)) unless
                $repo.trycall({$repo.dbobj.chainobj.table_exists}, :callframe(callframe));

            for @localdb.values -> $record {
                my @columns = self.check_local_record(:$record);

                next unless @columns && @columns.elems;

                my $created_unixtime = $!datepack.convert_http_response_date_to_unixtime(:date(@columns[2]));
                my $crated_offset    = (now - $created_unixtime).UInt;

                if $crated_offset >= $!expired_threshold {
                    # sprintf("skip commit id %d to remote table %s, offset %d", $record<id>, $table, $crated_offset).say;

                    next;
                }

                my $id = $record<id>.UInt;

                if $repo.trycall({$repo.dbobj.chainobj.id_exists(:$id)}, :callframe(callframe)) {
                    return False unless $repo.store(:$id, :@!fields, :@columns, :insert(False));
                }
                else {
                    return False unless $repo.store(:$id, :@!fields, :@columns, :insert(True));
                }

                sleep($!trxdelay) if $!trxdelay > 0;
            }
        }

        self.trxlog.push($repo.trxlog);
        $!used_endpoint = $repo.dbobj.chainobj.apiurl // q{};

        return $repo.trycall({$repo.dbobj.chainobj.wait_for_transactions(:hashes($repo.trxlog.map({$_<txhash>})))}, :callframe(callframe));
    }
    else {
        X::AdHoc.new(:payload(sprintf("database object for table %s on blockchain is failed", $table))).throw;
    }

    return False;
}

method translit(Str :$text!) returns Str {
    my $transliterated = q{};

    my $converter = {
        'а' => 'a',    'б' => 'b',    'в' => 'v',    'г' => 'g',    'д' => 'd',
        'е' => 'e',    'ё' => 'e',    'ж' => 'zh',   'з' => 'z',    'и' => 'i',
        'й' => 'y',    'к' => 'k',    'л' => 'l',    'м' => 'm',    'н' => 'n',
        'о' => 'o',    'п' => 'p',    'р' => 'r',    'с' => 's',    'т' => 't',
        'у' => 'u',    'ф' => 'f',    'х' => 'h',    'ц' => 'c',    'ч' => 'ch',
        'ш' => 'sh',   'щ' => 'sch',  'ь' => '',     'ы' => 'y',    'ъ' => '',
        'э' => 'e',    'ю' => 'yu',   'я' => 'ya',   'ң' => 'n',    'ү' => 'y',
        'ө' => 'o',

        'А' => 'A',    'Б' => 'B',    'В' => 'V',    'Г' => 'G',    'Д' => 'D',
        'Е' => 'E',    'Ё' => 'E',    'Ж' => 'Zh',   'З' => 'Z',    'И' => 'I',
        'Й' => 'Y',    'К' => 'K',    'Л' => 'L',    'М' => 'M',    'Н' => 'N',
        'О' => 'O',    'П' => 'P',    'Р' => 'R',    'С' => 'S',    'Т' => 'T',
        'У' => 'U',    'Ф' => 'F',    'Х' => 'H',    'Ц' => 'C',    'Ч' => 'Ch',
        'Ш' => 'Sh',   'Щ' => 'Sch',  'Ь' => '',     'Ы' => 'Y',    'Ъ' => '',
        'Э' => 'E',    'Ю' => 'Yu',   'Я' => 'Ya',   'Ң' => 'N',    'Ү' => 'Y',
        'Ө' => 'O'
    };

    my @chararray = $text.split(q{}, :skip-empty);

    for @chararray.values -> $char {
        next unless $char && $char.ord;

        if ($converter{$char}.defined) {
            $transliterated ~= $converter{$char};
        }
        else {
            $transliterated ~= $char;
        }
    }

    return $transliterated.trim;
}

method config(Str :$table!, Str :$path!, Hash :$overrideconfig, Bool :$build = True) returns Hash {
    if $build {
        return {
            module => {
                configuration => {
                    settings => {
                        storage => {
                            group => {
                                $table => {
                                    type => '0',
                                    strg => q{},
                                    extn => 'tnk',
                                    path => $path,
                                }
                            }
                        }
                    }
                }
            }
        };
    }
    else {
        my $config_filename = sprintf("%s/config.json", $path);

        if $config_filename.IO.f {
            my $payload;

            try {
                $payload = from-json($config_filename.IO.slurp);

                CATCH {
                    default {
                        my $e = .message;

                        X::AdHoc.new(:payload(sprintf("can not parse configuration file %s: %s", $config_filename, $e))).throw
                    }
                }
            }

            X::AdHoc.new(:payload(sprintf("table %s is not found in config", $table))).throw
                unless $payload<module><configuration><settings><storage><group>{$table} &&
                       $payload<module><configuration><settings><storage><group>{$table}.keys;

            if $overrideconfig {
                for $overrideconfig.kv -> $key, $value {
                    my $section = $payload<module><configuration><settings><storage><group>{$table}{$key};

                    if $section ~~ Hash && $section.keys {
                        if $value ~~ Hash && $value.keys {
                            $value.keys.map({$section{$_} = $value{$_}});
                        }
                    }
                    else {
                        $section = $value;
                    }

                    $payload<module><configuration><settings><storage><group>{$table}{$key} = $section;
                }

                # to-json($payload<module><configuration><settings><storage><group>{$table}).say;
            }

            return $payload;
        }
        else {
            X::AdHoc.new(:payload(sprintf("configuration file %s is not found", $config_filename))).throw
        }
    }
}

method addhashes(:@trxlog, Str :$table, Str :$path) returns UInt {
    return False unless @trxlog.elems;

    X::AdHoc.new(:payload(sprintf("table %s path %s is not found", $table, $path))).throw unless $path.IO.d;

    my $updates = 0;
    my $repo;
    my @providers;

    if !$!dbobj {
        X::AdHoc.new(:payload(sprintf("table %s path %s is not found", $table, $path))).throw unless $table && $path.IO.d;

        $repo = Ethelia::StationParser::AQICN::Repository::FileChain.new(:$table, :config(self.config(:$table, :$path)));
    }
    else {
        $repo = Ethelia::StationParser::AQICN::Repository::FileChain.new(:$!dbobj);
    }

    X::AdHoc.new(:payload(sprintf("table %s is not existed", $repo.dbobj.table))).throw unless $repo.dbobj.exists;

    if $!blobbing {
        X::AdHoc.new(:payload(sprintf("multiple blobs in transaction log %d", @trxlog.elems))).throw unless @trxlog.elems == 1;

        my $log = @trxlog.head;

        X::AdHoc.new(:payload(sprintf("no blob transaction details in log %s", $log.gist))).throw
            unless $log<caller> eq 'blob' && $log<id> == -1 && $log<txhash> ~~ m:i/^ 0x<xdigit>**64 $/;

        for $repo.select_all.values -> $record {
            my $payload;
            my @columns = self.check_local_record(:$record);

            next unless @columns && @columns.elems;

            try {
                $payload = from-json($!base64.decode-str(@columns[3]));

                CATCH {
                    default { next; }
                }
            }

            $payload<enrichment><transactionHash> = $log<txhash>;

            $updates++ if $repo.set(:id($record<id>.UInt), :@!fields, :@columns, :$payload);
        }
    }
    else {
        for @trxlog.values -> $log {
            next unless $log<id> > 0 && $log<txhash> ~~ m:i/^ 0x<xdigit>**64 $/;

            if (my @rows = $repo.dbobj.get({id => $log<id>.UInt})) {
                X::AdHoc.new(:payload(sprintf("inconsistent table %s state for %d id: found %d records", $repo.dbobj.table, $log<id>, @rows.elems))).throw
                    unless @rows.elems == 1;

                my $payload;
                my $record  = @rows.head;
                my @columns = self.check_local_record(:$record);

                next unless @columns && @columns.elems;

                try {
                    $payload = from-json($!base64.decode-str(@columns[3]));

                    CATCH {
                        default { next; }
                    }
                }

                $payload<enrichment><transactionHash> = $log<txhash>;

                $updates++ if $repo.set(:id($record<id>.UInt), :@!fields, :@columns, :$payload);
            }
        }
    }

    return $updates;
}

method check_local_record(:$record!) returns List {
    my @columns;

    return List.new unless $record ~~ Hash && $record<id> && $record<data>;

    try {
        @columns = $!base64.decode-str($record<data>).split(q{|}, :skip-empty);

        CATCH {
            default {
                ;
            }
        }
    }

    return (@columns.elems == @!fields.elems) ?? @columns !! List.new;
}
