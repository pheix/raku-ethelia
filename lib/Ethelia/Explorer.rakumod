unit class Ethelia::Explorer;

use Compress::Bzip2;
use JSON::Fast;
use MIME::Base64;
use Net::Ethereum;

# constant fields    = <id address code created payload topic compression>;
constant fields    = <address code created payload topic compression>;
constant functions = {
    insert => '0xb59e5025',
    set    => '0xf609bacf',
};

has Str $.abi = sprintf("%s/git/dcms-raku/conf/system/eth/PheixDatabase.abi", $*HOME);

method decode(Str :$function!, Str :$inputdata!) returns Hash {
    X::AdHoc.new(:payload(sprintf("unsupported function %s", $function))).throw unless $function && (functions{$function}:exists);

    X::AdHoc.new(:payload(sprintf("no abi %s found", $!abi))).throw unless $!abi && $!abi.IO.f;

    my $ne   = Net::Ethereum.new(:abi($!abi.IO.slurp));
    my $fabi = $ne.get_function_abi($function);

    X::AdHoc.new(:payload('no abi inputs')).throw unless $fabi<inputs> ~~ Array && $fabi<inputs>.elems;

    my $inputs = {}

    (my $marshalled = $inputdata) ~~ s/<{functions{$function}}>//;

    my buf8 $inputdata_buf  = $ne.hex2buf($marshalled);

    for $fabi<inputs>.kv -> $index, $input {
        my %enrichment;
        my $reference = :16($ne.buf2hex($inputdata_buf.subbuf($index * 32, 32)));

        if $input<type> ~~ /(string)|(bytes)/ {
            my %payload;

            my $length  = :16($ne.buf2hex($inputdata_buf.subbuf($reference, 32)));
            my $databuf = $inputdata_buf.subbuf($reference + 32, $length);

            if $input<name> eq 'rowdata' {
                my $plaindata = decompressToBlob($databuf).decode;
                my @records   = $plaindata.split(q{|}, :skip-empty);
                my $plainhash = Hash.new;

                for fields.kv -> $index, $value {
                    $plainhash{$value} = @records[$index];
                }

                %payload =
                    filechain => $plaindata,
                    payload   => from-json(MIME::Base64.new.decode-str($plainhash<payload>));
            }

            %enrichment =
                length    => $length,
                hexdata   => $ne.buf2hex($databuf),
                %payload
        }

        $inputs{$input<name>} = {
            type      => $input<type>,
            reference => $reference,
            %enrichment,
        }
    }

    return $inputs;
}
