unit class Ethelia::Advancer;

use Pheix::Model::Database::Access;
use Node::Ethereum::Keccak256::Native;
use Pheix::Model::Database::Blockchain::SendTx;
use Node::Ethereum::KeyStore::V3;
use Ethelia::Explorer;
use Ethelia::StationParser::AQICN::Repository::BlockChain;

has UInt $.blocksearchiters = 100;
has UInt $.blockshift       = 500;
has Str  $.explorerurlf     = "https://sepolia.etherscan.io/tx/%s";
has Bool $.debug            = False;

method advance(:@trxlog!, Pheix::Model::Database::Access :$dbobj!) returns Hash {
    X::AdHoc.new(:payload('input transaction log is empty')).throw unless @trxlog.elems;

    my $start = now;

    X::AdHoc.new(:payload('blockchain database object failure')).throw unless $dbobj && $dbobj.dbswitch == 1;

    X::AdHoc.new(:payload(sprintf("abi file name %s is not found", $dbobj.chainobj.get_path))).throw unless
        $dbobj.chainobj.get_path && $dbobj.chainobj.get_path.IO.f;

    my $trxdecoder = Ethelia::Explorer.new(:abi($dbobj.chainobj.get_path));

    $dbobj.chainobj.ethobj.keepalive = False;

    my $signer = Pheix::Model::Database::Blockchain::SendTx
        .new(
            :signerobj($dbobj.chainobj.sgnobj),
            :targetobj($dbobj.chainobj),
            :debug(True),
            :diag($dbobj.chainobj.diag)
        );

    X::AdHoc.new(:payload('no keystore found')).throw unless $signer.signerobj.config<keystore> && $signer.signerobj.config<keystore>.IO.f;

    X::AdHoc.new(:payload('no keystore password')).throw unless $signer.signerobj.ethobj.unlockpwd;

    return {
        transaction_num => self.process_transactions(:$trxdecoder, :$dbobj, :$signer, :@trxlog),
        runtime => (now - $start),
    };
}

method process_transactions(:$trxdecoder!, :$dbobj!, :$signer!, :@trxlog!) returns UInt {
    my UInt $processed_transactions = 0;
    my $ethobj = $dbobj.chainobj.ethobj;

    my $repo       = Ethelia::StationParser::AQICN::Repository::BlockChain.new(:$dbobj);
    my $privatekey = Node::Ethereum::KeyStore::V3
        .new(:keystorepath($signer.signerobj.config<keystore>))
        .decrypt_key(:password($signer.signerobj.ethobj.unlockpwd));

    my $actualnonce = $repo.trycall({$ethobj.eth_getTransactionCount(:data($dbobj.chainobj.ethacc), :tag('latest'))}, :callframe(callframe));

    X::AdHoc.new(:payload(sprintf("invalid actual nonce %s", $actualnonce))).throw unless $actualnonce > 0;

    sprintf("records in logfile %d, actual nonce %d", @trxlog.elems, $actualnonce.UInt).say if $!debug;

    for @trxlog.values -> $record {
        my $trxhash  = $record<txhash>;
        my $nonce    = $record<nonce>;
        my $function = $record<caller>;

        next unless $nonce >= $actualnonce && $trxhash ~~ m:i/^ 0x<xdigit>**64 $/;

        my $trx = $repo.trycall({$ethobj.eth_getTransactionByHash($trxhash)}, :callframe(callframe));

        if $trx.keys {
            my $input = $trxdecoder.decode(:$function, :inputdata($trx<input>));

            my %data =
                tabname   => $dbobj.table,
                rowid     => $input<rowid><reference>,
                rowdata   => $ethobj.hex2buf($input<rowdata><hexdata>),
                comp      => $input<comp><reference>,
                signature => $dbobj.chainobj.signaturefactory.sign(:message($input<rowdata><filechain>));

            my $mdata = $ethobj.marshal($function, %data);

            my %trx =
                from  => $dbobj.chainobj.ethacc,
                to    => $dbobj.chainobj.scaddr,
                nonce => $nonce,
                data  => $mdata;

            my $esimated_gas = $repo.trycall({$ethobj.eth_estimateGas(%trx)}, :callframe(callframe));
            my $fees         = $repo.trycall({$ethobj.get_fee_data}, :callframe(callframe));

            %trx<gas>                  = ($esimated_gas * $signer.gasprmult).Int;
            %trx<maxfeepergas>         = $fees<maxFeePerGas>;
            %trx<maxpriorityfeepergas> = $fees<maxPriorityFeePerGas>;

            my $updatedtrx;
            my $tried = 0;

            while (!$updatedtrx) {
                my %sign = $signer.sign_transaction(
                    :trx(%trx),
                    :privatekey($privatekey)
                );

                X::AdHoc.new(:payload('sign transaction failure')).throw unless %sign<raw> ~~ m:i/^ 0x<xdigit>+ $/;

                try {
                    $updatedtrx = $ethobj.eth_sendRawTransaction(:data(%sign<raw>));

                    CATCH {
                        default {
                            my $error = .message;

                            if $error ~~ /'replacement transaction underpriced'/ {
                                %trx<maxfeepergas> = %trx<maxfeepergas> * 2;
                                %trx<maxpriorityfeepergas> = %trx<maxpriorityfeepergas> * 2;

                                sprintf("replacement transaction underpriced: fees * 2 -> %d/%d", %trx<maxfeepergas>, %trx<maxpriorityfeepergas>).say if $!debug;
                            }
                            else {
                                .throw if ++$tried > $repo.retries;
                            }
                        }
                    }

                    sleep($repo.timeout);
                }
            }

            X::AdHoc.new(:payload('transaction is not mined')).throw unless $dbobj.chainobj.wait_for_transactions(:hashes([$updatedtrx]));

            sprintf("%03d - processed initial trx %s with existed data nonce=%d, function=%s: %s", ++$processed_transactions, $trxhash, $nonce, $function, $updatedtrx).say if $!debug;
        }
        else {
            X::AdHoc.new(:payload(sprintf("transaction %s nonce %s caller %s is not found in mempool", $record<txhash>, $record<nonce>, $record<caller>))).throw;
        }
    }

    return $processed_transactions;
}
