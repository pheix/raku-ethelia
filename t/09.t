use v6.d;
use Test;
use Test::Mock;

use JSON::Fast;

use Ethelia::StationParser::AQICN;
use Ethelia::StationParser::AQICN::Repository::BlockChain;
use Pheix::Model::Database::Access;

my $dbobj = mocked(Pheix::Model::Database::Access);

plan 3;

use-ok 'Ethelia::StationParser::AQICN::Repository::BlockChain', 'Ethelia::StationParser::AQICN::Repository::BlockChain is used ok';

subtest {
    my $repo = Ethelia::StationParser::AQICN::Repository::BlockChain.new(:$dbobj, :diag(sub _diag(Str:$m){diag($m)}));
    dies-ok { $repo.trycall({die 'no callframe!'}) }, 'died after a few retries without callframe';
    dies-ok { $repo.trycall({die 'opps!'}, :callframe(callframe)) }, 'died after a few retries with callframe';
}, 'generic check';

if !%*ENV<TESTBLOCKCHAIN> {
    skip-rest('TESTBLOCKCHAIN was not set');

    exit;
}

subtest {
    plan 1;

    my $token = %*ENV<AQICNTOKEN> // q{};

    ok $token && $token.chars, 'AQICN token';

    my $table          = 'tst_table';
    my $path           = sprintf("%s/git/dcms-raku/conf", $*HOME);
    my $overrideconfig = {path => sprintf("%s/git/dcms-raku/conf/system/eth", $*HOME)};

    my $provider = Ethelia::StationParser::AQICN.new(:$token);

    my $config = $provider.config(:$table, :$path, :$overrideconfig, :build(False));
    my $repo   = Ethelia::StationParser::AQICN::Repository::BlockChain.new(:$table, :$config);

    if $repo.dbobj.dbswitch == 1 {
        my %ret = $repo.trycall({$repo.dbobj.chainobj.table_create(:waittx(True))});

        diag(to-json(%ret));
    }
}, 'Try safe blockchain call';

done-testing;
