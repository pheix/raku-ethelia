use v6.d;
use Test;

use Ethelia::Component;

plan 2;

use-ok 'Ethelia::Component', 'Ethelia::Component is used ok';

subtest {
    plan 3;

    my $default = Ethelia::Component.new.build;
    my $regular = Ethelia::Component.new.build(:key('regular'));
    my $search  = Ethelia::Component.new.build(:key('search'));

    ok $default ne q{}, 'default component';
    ok $regular ne q{}, 'regular component';
    ok $search  ne q{}, 'search component';

    diag($default);
    diag($regular);
    diag($search);
}, 'Check component build';

done-testing;
