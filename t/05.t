use v6.d;
use Test;

use Ethelia;

constant user     = %*ENV<MATRIXUSER> // q{};
constant password = %*ENV<MATRIXPASS> // q{};
constant roomid   = %*ENV<MATRIXROOM> // q{};
constant tgtoken  = %*ENV<TELEGRAMTOKEN> // q{};
constant tgchatid = %*ENV<TELEGRAMCHAT> // q{};

plan 2;

subtest {
    plan 7;

    if !(user ne q{} && password ne q{} && roomid ne q{}) {
        skip 'No Matrix access credentials are given', 7;
    }
    else {
        my $ethelia = Ethelia.new(:confpath('custom-config/storage'));

        ok $ethelia, 'ethelia object';

        $ethelia.jsonobj.set_group_setting($ethelia.get_name, 'notifier', 'matrix', 'user', user, :temporary(True));
        $ethelia.jsonobj.set_group_setting($ethelia.get_name, 'notifier', 'matrix', 'password', password, :temporary(True));
        $ethelia.jsonobj.set_group_setting($ethelia.get_name, 'notifier', 'matrix', 'roomid', roomid, :temporary(True));

        my $payload = {
            message => sprintf("dummy notification message from CI/CD job %d", %*ENV<CI_JOB_ID> // 0),
            msgtype => 'm.text',
            body => q{},
            format => 'org.matrix.custom.html',
            formatted_body => '<h3>Header</h3><p>Lorem ipsum</p>',
            code => '01982-1',
            topic => 'blockchain_event_1',
            payload => {
                return => 1,
                score => 2022,
                result => 'success',
                details => {
                    items => [
                        1.87,
                        2.16,
                        3.52
                    ],
                    status => True
                }
            }
        };

        my $sent_test1_rc = False;
        my $sent_test2_rc = False;
        my $sent_test3_rc = False;

        lives-ok { $sent_test1_rc = $ethelia.send_notification(:topic('pheixfeedback'), :$payload, :key('message')) }, 'send via metrix lives - message by key';
        lives-ok { $sent_test2_rc = $ethelia.send_notification(:topic('pheixfeedback'), :$payload) }, 'send via metrix lives - full payload';
        lives-ok { $sent_test3_rc = $ethelia.send_notification(:topic('faketopic'), :$payload) }, 'send via metrix lives - topic filtering';

        ok $sent_test1_rc, 'notification for case no.1 is sent';
        ok $sent_test2_rc, 'notification for case no.2 is sent';
        ok !$sent_test3_rc, 'notification for case no.3 is skipped';
    }
}, 'Send notification to Matrix';

subtest {
    plan 5;

    if !(tgtoken ne q{} && tgchatid ne q{}) {
        skip 'No Telegram access credentials are given', 5;
    }
    else {
        my $ethelia = Ethelia.new(:confpath('custom-config/storage'));

        ok $ethelia, 'ethelia object';

        $ethelia.jsonobj.set_setting($ethelia.get_name, 'defaultnotifier', 'value', 'telegram', :temporary(True));
        $ethelia.jsonobj.set_group_setting($ethelia.get_name, 'notifier', 'telegram', 'token', tgtoken, :temporary(True));
        $ethelia.jsonobj.set_group_setting($ethelia.get_name, 'notifier', 'telegram', 'chatid', tgchatid, :temporary(True));

        my $payload = {
            message => sprintf("dummy notification message from CI/CD job %d", %*ENV<CI_JOB_ID> // 0),
            msgtype => 'm.text',
            body => q{},
            format => 'org.matrix.custom.html',
            formatted_body => '<h3>Header</h3><p>Lorem ipsum</p>',
            code => '01982-1',
            topic => 'blockchain_event_1',
            payload => {
                return => 1,
                score => 2022,
                result => 'success',
                details => {
                    items => [
                        1.87,
                        2.16,
                        3.52
                    ],
                    status => True
                }
            }
        };

        my $sent_test1_rc = False;
        my $sent_test2_rc = False;

        lives-ok { $sent_test1_rc = $ethelia.send_notification(:topic('pheixfeedback'), :$payload, :key('message')) }, 'send via telegram lives - message by key';
        lives-ok { $sent_test2_rc = $ethelia.send_notification(:topic('pheixfeedback'), :$payload) }, 'send via telegram lives - full payload';

        ok $sent_test1_rc, 'notification for case no.1 is sent';
        ok $sent_test2_rc, 'notification for case no.2 is sent';
    }
}, 'Send notification to Telegram';

done-testing;
