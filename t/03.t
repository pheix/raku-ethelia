use v6.d;
use Test;

use JSON::Fast;

use Pheix::Utils;
use Pheix::View::Pages;
use Pheix::Model::Database::Access;

use Ethelia;

plan 4;

subtest {
    plan 1;

    my $ethelia      = Ethelia.new(:confpath('custom-config'));
    my $moduleconfig = $ethelia.jsonobj;

    ok $moduleconfig, 'fetch configuration from module json->tnk bridge';
}, 'Check external config';

subtest {
    plan 2;

    throws-like { Ethelia.new(:confpath(Str)) },
        Exception,
        message => /'no config file for ExtensionEthelia addon found'/,
        'throws exception on empty config path';

    throws-like { Ethelia.new(:confpath('fake/path/not/found')) },
        Exception,
        message => /'no config file for ExtensionEthelia addon found'/,
        'throws exception on invalid config path';
}, 'Check exception on wrong path';

subtest {
    plan 3;

    my $eth = Ethelia.new(:confpath('custom-config'));

    my %initial_setup = $eth.init_json_config(:path($eth.confpath), :skipexternal(True)).get_entire_config;

    %initial_setup<module><configuration><settings><external-config-storage><value> = 'fake/path';

    my $testpath = $eth.confpath ~ '/storage/test/' ~ $eth.get_name ~ '/config.json';

    ok spurt($testpath, to-json(%initial_setup)), 'save patched config';

    throws-like { Ethelia.new(:confpath($eth.confpath ~ '/storage/test/')) },
        Exception,
        message => /'no database for external config in fake/path'/,
        'throws exception on missed table';

    ok unlink($testpath), 'delete test config file';
}, 'Check exception on wrong path';

subtest {
    plan 5;

    my $eth = Ethelia.new(:confpath('custom-config'));

    my %initial_setup = $eth.init_json_config(:path($eth.confpath), :skipexternal(True)).get_entire_config;

    %initial_setup<module><configuration><settings><external-config-storage><value> = 'test/ExtensionEthelia/BlankTab';

    my $cloned = %initial_setup<module><configuration><settings><storage><group><ExtensionEthelia>;

    %initial_setup<module><configuration><settings><storage><group><test/ExtensionEthelia/BlankTab> = $cloned;

    my $testpath = $eth.confpath ~ '/storage/test/' ~ $eth.get_name ~ '/config.json';
    my $dbpath   = $eth.confpath ~ '/storage/test/' ~ $eth.get_name ~ '.tnk';

    ok spurt($testpath, to-json(%initial_setup)), 'save patched config';
    ok spurt($dbpath, q{}), 'save blank database';

    throws-like { Ethelia.new(:confpath($eth.confpath ~ '/storage/test/')) },
        Exception,
        message => /'no database for external config in test/ExtensionEthelia/BlankTab'/,
        'throws exception on missed table';

    ok unlink($testpath), 'delete test config file';
    ok unlink($dbpath), 'delete test database file';
}, 'Check exception on blank config path';

done-testing;
