use v6.d;
use Test;

use Ethelia;
use Ethelia::Test::Requirements;

plan 3;

use-ok 'Ethelia', 'Ethelia is used ok';
use-ok 'Ethelia::Test::Requirements', 'Ethelia::Test::Requirements is used ok';

my $ethelia      = Ethelia.new(:confpath('custom-config/storage'));
my $requirements = Ethelia::Test::Requirements.new(:name($ethelia.get_name));
my $moduleconfig = $requirements.init_json_config;

ok $moduleconfig, 'config is not null';

diag($moduleconfig.get_entire_config(:addon($ethelia.get_name),:nocache(True)).gist);

done-testing;
