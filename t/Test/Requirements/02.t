use v6.d;
use Test;

use JSON::Fast;

use Pheix::Utils;
use Pheix::View::Pages;
use Pheix::Model::Database::Access;

use Ethelia;
use Ethelia::Test::Requirements;

plan 5;

use-ok 'Ethelia', 'use Ethelia';
use-ok 'Ethelia::Test::Requirements', 'use Ethelia::Test::Requirements';

my $ethelia      = Ethelia.new(:confpath('custom-config/storage'));
my $requirements = Ethelia::Test::Requirements.new(:name($ethelia.get_name));
my $moduleconfig = $requirements.init_json_config;

ok $moduleconfig, 'fetch configuration from module json file';

my $database = Pheix::Model::Database::Access.new(
    :table($ethelia.get_name),
    :fields(List.new),
    :jsonobj($moduleconfig)
);

is $database.table, $ethelia.get_name, 'create database object';

my $setup = from-json(
    Pheix::View::Pages
        .new(:utilobj(Pheix::Utils.new(:jsonobj($moduleconfig))), :jsonobj($moduleconfig))
        .raw_pg(:table($ethelia.get_name), :database($database))
);

ok $setup.keys.elems, 'fetch configuration from database';

diag($requirements.init_json_config(:setup($setup)).get_entire_config.gist);

done-testing;
