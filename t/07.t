use v6.d;
use Test;
use Test::Mock;

use Ethelia;
use Pheix::Addons::Embedded::Admin;
use Pheix::Controller::Basic;
use Pheix::Model::Database::Access;
use Ethelia::StationParser::AQICN;
use JSON::Fast;
use MIME::Base64;

class mockeAuthGW {
    method validate_on_blockchain {
        return { status => True, addr => '0x37594ae81970eeddc5bae01f70029bd022d4519f' };
    }
}

my $mocked_admin = mocked(
    Pheix::Addons::Embedded::Admin,
    returning => {
        get => mocked(
            Pheix::Addons::Embedded::Admin,
            returning => { get_authnode => mockeAuthGW.new }
        )
    }
);

plan 5;

my $ctrl = Pheix::Controller::Basic.new(
    :apirobj(Nil),
    :jsonobj(Pheix::Model::JSON.new(:addonpath('custom-config/storage')).set_entire_config(:addon('ExtensionEthelia'))),
    :mockedfcgi(Pheix::Test::FastCGI.new),
    :test(True),
    :addons({'Pheix::Addons::Embedded::Admin' => { objct => $mocked_admin }})
);

my $ethelia = Ethelia.new(
    :confpath('custom-config/storage'),
    :pheixabi(sprintf("%s/git/dcms-raku/conf/system/eth/PheixDatabase.abi", $*HOME))
).get(:ctrl($ctrl));

is $ethelia.get_name, 'ExtensionEthelia', 'Ethelia extension';

use-ok 'Ethelia::StationParser::AQICN', 'Ethelia::StationParser::AQICN is used ok';

subtest {
    plan 2;

    my $provider = Ethelia::StationParser::AQICN.new;
    my $table    = 'tab';
    my $path     = './t/fake/';

    throws-like
        {
            $provider.synclocal(:stations(List.new({station => {}})), :$table, :$path);
        },
        Exception,
        message => sprintf("table %s path %s is not found", $table, $path),
        'dies on fake table path';

    throws-like
        {
            $provider.synclocal(:stations(List.new({station => {}})), :$table, :path('./t/custom-config/storage/test'));
        },
        Exception,
        message => sprintf("table %s is not existed", $table),
        'dies on unexisted table';

}, 'Sync exceptions';

subtest {
    plan 1;

    my $data = {
        ru => 'абвгдеёжзийклмнопрстуфхцчшщьыъэюяңүө',
        en => 'abvgdeezhziyklmnoprstufhcchshschyeyuyanyo'
    };

    my $provider = Ethelia::StationParser::AQICN.new;

    is $provider.translit(:text($data<ru>)), $data<en>, 'transliteration';

}, 'Transliteration';

subtest {
    plan 4;

    my @stations;
    my $token = %*ENV<AQICNTOKEN> // q{};

    ok $token && $token.chars, 'AQICN token';

    my $provider = Ethelia::StationParser::AQICN.new(:$token);

    ok $provider, 'station provider object';

    my @raw_stations = $provider.stations;

    ok @raw_stations && @raw_stations.elems, 'AQICN stations';

    for @raw_stations -> $station {
        @stations.push($station) unless @stations.grep({ $station<title> eq $_<title> }).elems.Bool;
    }

    # diag(to-json(@stations));

    subtest {
        plan 11;

        my @origin;
        my $deleted_rows = 0;

        my $dbobj = Pheix::Model::Database::Access.new(
            :table($ethelia.get_name),
            :fields(List.new),
            :jsonobj($ethelia.jsonobj),
            :test(True),
        );

        ok  $dbobj, 'init database';

        if $dbobj.exists {
            ok $dbobj.remove_all && $dbobj.chainobj.table_create<status>, 'initial database creanup';
        }
        else {
            ok  $dbobj.chainobj.table_create<status>, 'create table';
        }

        nok get_all(:$dbobj).elems, 'blank database';
        ok  $provider.synclocal(:@stations, :table($dbobj.table), :path($dbobj.dbpath.IO.dirname.Str)), 'populate database with stations';

        @origin = get_all(:$dbobj);

        # diag(@origin.map({ $_<id>.UInt }).sort.join(','));

        is @origin.elems, @stations.elems, 'records in database';

        for @origin.values -> $row {
            next unless $row<id> % 2 == 0;

            $deleted_rows++ if $dbobj.remove({id => $row<id>});
        }

        is get_all(:$dbobj).elems, @origin.elems - $deleted_rows, 'delete from database';

        # diag(get_all(:$dbobj).map({ $_<id>.UInt }).sort.join(','));

        ok $provider.synclocal(:@stations, :table($dbobj.table), :path($dbobj.dbpath.IO.dirname.Str)), 'populate database with new stations';

        is get_all(:$dbobj).elems, @stations.elems, 'records in database';

        # diag(get_all(:$dbobj).map({ $_<id>.UInt }).sort.join(','));

        is-deeply @stations.sort, to_stations(:$dbobj).sort, 'stations data';
        is-deeply @stations.sort, brute_force(:@stations, :$dbobj, :$provider).sort, 'stations data after database bruteforce';

        ok $dbobj.remove_all, 'drop database';
    }, 'sync stations'
}, 'AQICN stations';

done-testing;

sub get_all(Pheix::Model::Database::Access :$dbobj!) returns List {
    return $dbobj.get_all(:fast(True), :withcomp(True));
}

sub to_stations(Pheix::Model::Database::Access :$dbobj!) returns List {
    my @stations;

    for get_all(:$dbobj).values -> $record {
        my @columns = MIME::Base64.decode-str($record<data>).split(q{|}, :skip-empty);
        my $payload = from-json(MIME::Base64.decode-str(@columns[3]));

        @stations.push(
            {
                title => $payload<enrichment><title>,
                sensorvalue => $payload<sensorvalue>,
                profile => $payload<enrichment><profile>,
                addr => @columns.head,
                position => $payload<enrichment><position>
            }
        );
    }

    return @stations;
}

sub brute_force(
    :@stations!,
    Pheix::Model::Database::Access :$dbobj!,
    Ethelia::StationParser::AQICN  :$provider!,
    UInt :$iters = 50
) returns List {
    for ^$iters -> $iter {
        my @current = get_all(:$dbobj);

        my @indexes;
        my $items = @current.elems.rand.Int || 1;

        for ^$items {
            my $item = @current.elems.rand.Int || 1;

            next if @indexes.grep({$_ == $item}).elems;

            @indexes.push($item);
        }

        @indexes.push($items) unless @indexes.elems;

        for @indexes.values -> $index {
            $dbobj.remove({id => @current[$index]<id>});
        }

        $provider.synclocal(:@stations, :table($dbobj.table), :path($dbobj.dbpath.IO.dirname.Str));
    }

    return to_stations(:$dbobj);
}
