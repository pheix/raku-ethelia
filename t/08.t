use v6.d;
use Test;
use Test::Mock;

use JSON::Fast;
use MIME::Base64;

use Ethelia::Advancer;
use Ethelia::StationParser::AQICN;
use Ethelia::StationParser::AQICN::Repository::BlockChain;
use Pheix::Model::Database::Access;
use Pheix::Model::JSON;
use Pheix::Test::Blockchain;

constant debug    = False;
constant localtab = 'tst_table';
constant tstobj   = Pheix::Test::Blockchain.new(:locstorage(localtab), :genwords(300));

plan 2;

use-ok 'Ethelia::StationParser::AQICN::Repository::BlockChain', 'Ethelia::StationParser::AQICN::Repository::BlockChain is used ok';

if !%*ENV<TESTBLOCKCHAIN> {
    skip-rest('TESTBLOCKCHAIN was not set');

    exit;
}

subtest {
    plan 12;

    my $token = %*ENV<AQICNTOKEN> // q{};

    ok $token && $token.chars, 'AQICN token';

    my $table    = 'tst_table';
    my $path     = sprintf("%s/git/dcms-raku/conf", $*HOME);
    my $provider = Ethelia::StationParser::AQICN.new(:$token, :debug(debug));
    my @stations = $provider.stations;
    my $localdbobj = Pheix::Model::Database::Access.new(
        :$table,
        :fields(List.new),
        :jsonobj(Pheix::Model::JSON.new.set_entire_config(:setup($provider.config(:$table, :path('custom-config/storage/test'))))),
        :test(True),
    );

    ok $localdbobj, 'init database';

    if $localdbobj.chainobj.table_exists {
        ok $localdbobj.remove_all, 'initial drop database';
    }
    else {
        skip 'no initial drop is needed', 1;
    }

    ok $localdbobj.chainobj.table_create<status>, 'create table';
    ok @stations && @stations.elems, 'AQICN stations';
    ok $provider.synclocal(:@stations, :table($localdbobj.table), :path($localdbobj.dbpath.IO.dirname.Str)), 'populate database with stations';

    # my @localdb = get_all(:$localdbobj).pick(10);
    my @localdb = get_all(:$localdbobj);

    diag(@localdb.map({ $_<id>.UInt }).sort.join(','));

    my $overrideconfig = {
        path => sprintf("%s/git/dcms-raku/conf/system/eth", $*HOME),
        sign => $table,
        conf => {
            keystore => 't/data/keystore/geth/ks.json',
        }
    };

    my $config     = $provider.config(:$table, :$path, :$overrideconfig, :build(False));
    my $remotebobj = Pheix::Model::Database::Access.new(
        :$table,
        :fields(List.new),
        :jsonobj(Pheix::Model::JSON.new.set_entire_config(:setup($config))),
        :test(True),
        :debug(debug)
    );

    my $repo = Ethelia::StationParser::AQICN::Repository::BlockChain.new(:dbobj($remotebobj));

    subtest {
        plan 2;

        is $repo.dbobj.chainobj.unlock_account, True, 'account unlock';

        my $tabs_on_blockchain = $repo.dbobj.chainobj.count_tables;

        if $tabs_on_blockchain {
            is tstobj.drop_all(:dbobj($repo.dbobj)), True, 'init drop';
        }
        else {
            is $tabs_on_blockchain, 0, 'empty blockchain';
        }
    }, 'prepare blockchain';

    ok $provider.syncremote(:@localdb, :$table, :$path, :$overrideconfig), 'initial blockchain database sync';

    subtest {
        plan 10;

        my @hashes;
        my @remotedb = $repo.select_all;

        is-deeply @remotedb.map({ $_<id>.UInt }).sort, @localdb.map({ $_<id>.UInt }).sort, 'check data from blockchain database by id';
        is-deeply @remotedb.map({ $_<data> }).sort, @localdb.map({ MIME::Base64.decode-str($_<data>) }).sort, 'check data from blockchain database by content';

        $repo.dbobj.chainobj.nonce = 0; # sync nonce between $repo and $provider

        for @remotedb.values -> $row {
            next unless $row<id> % 2 == 0;

            if (my %ret = $repo.dbobj.chainobj.delete(:id($row<id>), :waittx(False))) {
                @hashes.push(%ret<txhash>) if %ret<status>;
            }
        }

        ok $repo.dbobj.chainobj.wait_for_transactions(:@hashes), 'delete from blockchain database';

        my @remotedb_ondelete = $repo.select_all;

        diag(@remotedb_ondelete.map({ $_<id>.UInt }).sort.join(','));

        is @remotedb_ondelete.elems, @remotedb.elems - @hashes.elems, 'valid records num in blockchain database';
        ok $provider.syncremote(:@localdb, :$table, :$path, :$overrideconfig), 'blockchain database sync after delete';

        my @remotedb_onresync = $repo.select_all;

        is-deeply @remotedb_onresync.map({ $_<id>.UInt }).sort, @localdb.map({ $_<id>.UInt }).sort, 'check data from blockchain database by id after resync';
        is-deeply @remotedb_onresync.map({ $_<data> }).sort, @localdb.map({ MIME::Base64.decode-str($_<data>) }).sort, 'check data from blockchain database by content after resync';

        is $provider.trxlog.elems, 2, 'log batches';
        is $provider.trxlog.head.elems, @remotedb_onresync.elems, 'log 1st batch';
        is $provider.trxlog.tail.elems, @remotedb_onresync.elems, 'log 2nd batch';

        diag(to-json($provider.trxlog.tail.sort({%^a<nonce>.UInt <=> %^b<nonce>.UInt})));
    }, 'check blockchain';

    subtest {
        plan 1;

        my $advancer = Ethelia::Advancer.new(:table($repo.dbobj.table));
        my %result   = $advancer.advance(:trxlog($provider.trxlog.tail), :dbobj($repo.dbobj));

        ok %result.keys, 'advancer stats';
        diag(to-json(%result));
    }, 'call advancer';

    subtest {
        plan 2;

        my $size   = ($provider.trxlog.tail.elems / 2).Int;
        my @trxlog = $provider.trxlog.tail.pick($size);

        $provider.addhashes(:@trxlog, :table($localdbobj.table), :path($localdbobj.dbpath.IO.dirname.Str));

        is check_hashes(:$localdbobj), $size, 'check hashes (set of records)';

        $provider.addhashes(:trxlog($provider.trxlog.tail), :table($localdbobj.table), :path($localdbobj.dbpath.IO.dirname.Str));

        is check_hashes(:$localdbobj), $provider.trxlog.tail.elems, 'check hashes (all records)';
    }, 'update hashes';

    ok $localdbobj.remove_all, 'drop database';
}, 'Sync local and blockchain databases';

done-testing;

sub get_all(Pheix::Model::Database::Access :$localdbobj!) returns List {
    return $localdbobj.get_all(:fast(True), :withcomp(True));
}

sub check_hashes(Pheix::Model::Database::Access :$localdbobj!) returns UInt {
    my $hashes_num = 0;

    for get_all(:$localdbobj).values -> $record {
        my @columns = MIME::Base64.decode-str($record<data>).split(q{|}, :skip-empty);
        my $payload = from-json(MIME::Base64.decode-str(@columns[3]));

        $hashes_num++ if $payload<enrichment><transactionHash>:exists;
    }

    return $hashes_num;
}
