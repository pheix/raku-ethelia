use v6.d;
use Test;
use Test::Mock;

use Ethelia::Advancer;
use Pheix::Model::Database::Access;

plan 3;

use-ok 'Ethelia::Advancer', 'Ethelia::Advancer is used ok';

if !%*ENV<TESTBLOCKCHAIN> {
    skip-rest('TESTBLOCKCHAIN was not set');

    exit;
}

my $dbobj = mocked(Pheix::Model::Database::Access);

subtest {
    plan 1;

    my $advancer = Ethelia::Advancer.new;

    throws-like
        {
            $advancer.advance(:trxlog([]), :$dbobj);
        },
        Exception,
        message => 'input transaction log is empty',
        'dies on empty log';
}, 'advance failed transaction';

subtest {
    plan 1;

    my $advancer = Ethelia::Advancer.new;

    ok $advancer, 'advancer object'
}, 'advance not mined transaction';

done-testing;
