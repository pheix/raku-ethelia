use v6.d;
use Test;
use Test::Mock;

use Ethelia;
use Pheix::Addons::Embedded::Admin;
use Pheix::Model::Database::Access;
use Pheix::Test::FastCGI;
use Pheix::Controller::Basic;
use MIME::Base64;
use JSON::Fast;

use Pheix::Test::Blockchain;
use Pheix::Test::BlockchainComp::Helpers;

constant payload = {sample => {data => [1, 2, 3, 4]}};
constant setraw  = "0xf609bacf00000000000000000000000000000000000000000000000000000000000000a00000000000000000000000000000000000000000000000000000000065c8fc2800000000000000000000000000000000000000000000000000000000000000e0000000000000000000000000000000000000000000000000000000000000000100000000000000000000000000000000000000000000000000000000000002c00000000000000000000000000000000000000000000000000000000000000010457874656e73696f6e457468656c69610000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001ad425a68363141592653592140d91400000d9f8040067ff23fffbff03ffffff430015332222993519ea136a66a34f447a8d32190006868d3268229faa9e9a9ea794f53d4d1ea7a4d34680001ea000001a6929ea34d4cd4f53f253d3534f53119335323d34ca34c41a6d408cc8571ea6899032592afb7dafca02a3c2dd58358d7ec114f4a27577e5050a4063248f8052a37cc6c263ad0d948a89c94749188d0f4738e4b59d64dbb1608000010684e93d19240047b2c6003f129709dd96cb97a1c5956e9ac80dccd5905728ab792872052504c56a77c7280f3b57cefa26103ee05161aca4f687e14c7d4c4827980cb918748117bb48843e11906416a5f49543a59094029e6055b6997a0babead1d4982c2a4e10644c53f41cf794566c0ee7b5f8608513186e51f825bee2e4321879fbad73574dc6464b1f0ef6f1bfc356b3b8bf98b501050100f438d996ad1874f1e7b42d3b6c4e4330beec1762e16a8a7ea69201e118509a542d524156282f4acd50fca0cf2be210b2264ec0a4b76623da966b41dd903dcf2e18341f79bda0cfba8e4b1998076b7406703a94a08da23cd8b7d5badca767f1772453850902140d91400000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000008430783041344241353045453335423841313233333234353842313541374546383546413543363136444536343930434546383531363041363143353845353237343042464639334238343030443845434242354630464235433534354435383332313434434643413737324542343637463330324236383036424432443630323131303100000000000000000000000000000000000000000000000000000000";

class mockeAuthGW {
    method validate_on_blockchain {
        return { status => True, addr => '0x37594ae81970eeddc5bae01f70029bd022d4519f' };
    }
}

my $mocked_admin = mocked(
    Pheix::Addons::Embedded::Admin,
    returning => {
        get => mocked(
            Pheix::Addons::Embedded::Admin,
            returning => { get_authnode => mockeAuthGW.new }
        )
    }
);

plan 5;

use-ok 'Ethelia', 'Ethelia is used ok';

my $ctrl = Pheix::Controller::Basic.new(
    :apirobj(Nil),
    :jsonobj(Pheix::Model::JSON.new(:addonpath('custom-config/storage')).set_entire_config(:addon('ExtensionEthelia'))),
    :mockedfcgi(Pheix::Test::FastCGI.new),
    :test(True),
    :addons({'Pheix::Addons::Embedded::Admin' => { objct => $mocked_admin }})
);

my $ethelia = Ethelia.new(
    :confpath('custom-config/storage'),
    :pheixabi(sprintf("%s/git/dcms-raku/conf/system/eth/PheixDatabase.abi", $*HOME))
).get(:ctrl($ctrl));

is $ethelia.get_name, 'ExtensionEthelia', 'Ethelia has right extension name';

subtest {
    plan 16;

    my $dbobj = Pheix::Model::Database::Access.new(
        :table($ethelia.get_name),
        :fields(<id address code topic payload>),
        :jsonobj($ethelia.jsonobj)
    );

    ok $dbobj, 'init database';

    if !$dbobj.exists {
        nok $dbobj.exists, 'event table not found';
    }
    else {
        ok $dbobj.remove_all, 'initial drop table';
    }

    ok ($dbobj.chainobj.table_create(:t($ethelia.get_name)))<status>, 'create event table';

    my %rc = store_event();

    diag(%rc.gist);

    is %rc<result>, 'success', 'insert event';
    is-deeply  %rc<chaindetails>, Array.new.push({table => $ethelia.get_name, id => 1}), 'chain details';
    ok %rc<notification>.defined, 'notification';

    for ^5 -> $code {
        store_event(:$code);
    }

    my @events = $ethelia.get_event_list;

    is @events.elems, 5, 'events found';
    ok @events[0].values, 'default event values';
    is @events[0][1], 1982, 'default event code';
    is-deeply from-json(MIME::Base64.new.decode-str(@events[0][3])), payload, 'default event payload';

    my $new_payload = {update => payload};

    ok store_event(:payload($new_payload)).keys, 'update default event';

    @events = $ethelia.get_event_list;

    is @events.elems, 5, 'no new events found after update';
    is @events[0][1], 1982, 'default event code after update';
    is-deeply from-json(MIME::Base64.new.decode-str(@events[0][3])), $new_payload, 'default event updated payload after update';

    my $extension_api_content = $ethelia.extention_api_content(:match(%(details => {validate => {scope => 'full'}})));

    ok $extension_api_content.keys, 'extension content';

    # diag($extension_api_content.gist);

    ok $dbobj.remove_all, 'drop table';
}, 'Check events storing';

subtest {
    plan 21;

    my $link    = 'linked/ExtensionEthelia';
    my $jsonobj = $ethelia.jsonobj.clone;

    $jsonobj.set_group_setting($ethelia.get_name, 'storage', $ethelia.get_name, 'link', $link, :temporary(True));

    my @databases;

    for $ethelia.get_tables(:t($ethelia.get_name)) -> $t {
        my $dbobj = Pheix::Model::Database::Access.new(
            :table($t),
            :fields(<id address code topic payload>),
            :jsonobj($ethelia.jsonobj)
        );

        ok $dbobj, sprintf("init database for %s", $dbobj.table);

        if !$dbobj.exists {
            nok $dbobj.exists, 'event table not found';
        }
        else {
            ok $dbobj.remove_all, 'initial drop table';
        }

        ok ($dbobj.chainobj.table_create(:t($dbobj.table)))<status>, sprintf("create event table %s", $dbobj.table);

        @databases.push($dbobj);
    }

    my %rc = store_event();

    is @databases.elems, 2, 'found 2 databases';

    is %rc<result>, 'success', 'insert event';
    is-deeply  %rc<chaindetails>, Array.new.push(
        {
            table => $link,
            id => 1
        },
        {
            table => $ethelia.get_name,
            id => 1
        },
    ), 'chain details';
    ok %rc<notification>.defined, 'notification';

    for ^5 -> $code {
        store_event(:$code);
    }

    my @events = $ethelia.get_event_list;

    is @events.elems, 5, 'events found';
    ok @events[0].values, 'default event values';
    is @events[0][1], 1982, 'default event code';
    is-deeply from-json(MIME::Base64.new.decode-str(@events[0][3])), payload, 'default event payload';

    my $new_payload = {update => payload};

    ok store_event(:payload($new_payload)).keys, 'update default event';

    @events = $ethelia.get_event_list;

    is @events.elems, 5, 'no new events found after update';
    is @events[0][1], 1982, 'default event code after update';
    is-deeply from-json(MIME::Base64.new.decode-str(@events[0][3])), $new_payload, 'default event updated payload after update';

    my $extension_api_content = $ethelia.extention_api_content(:match(%(details => {validate => {scope => 'full'}})));

    ok $extension_api_content.keys, 'extension content';

    # diag($extension_api_content.gist);

    for @databases -> $dbobj {
        ok $dbobj.remove_all, sprintf("drop table %s", $dbobj.table);
    }
}, 'Check events storing to linked table';

subtest {
    plan 14;

    my $link    = 'blockchain/ExtensionEthelia';
    my $tstobj  = Pheix::Test::Blockchain.new(:locstorage($link), :genwords(200));
    my $helpobj = Pheix::Test::BlockchainComp::Helpers.new(
        :testnet($link),
        :localtab($link),
        :$tstobj
    );

    my $jsonobj = $ethelia.jsonobj.clone;

    $jsonobj.set_group_setting($ethelia.get_name, 'storage', $ethelia.get_name, 'link', $link, :temporary(True));

    my @databases;

    for $ethelia.get_tables(:t($ethelia.get_name)) -> $t {
        my $dbobj = Pheix::Model::Database::Access.new(
            :table($t),
            :fields(<id address code topic payload>),
            :jsonobj($ethelia.jsonobj)
        );

        ok $dbobj, sprintf("init database for %s (chain type %d)", $dbobj.table, $dbobj.dbswitch);

        if $dbobj.dbswitch == 1 {
            $helpobj.dbobj_tweak(:$dbobj, :testnet($dbobj.table));
        }

        if !$dbobj.exists {
            nok $dbobj.exists, 'event table not found';
        }
        else {
            ok $dbobj.remove_all, 'initial drop table';
        }

        todo 'Possible no Ethereum node';
        ok ($dbobj.chainobj.table_create(:t($dbobj.table)))<status>, sprintf("create event table %s", $dbobj.table);

        @databases.push($dbobj);
    }

    is @databases.elems, 2, 'found 2 databases';

    my $blockchain_db = @databases.grep({ $_.dbswitch == 1}).head;

    if $blockchain_db {
        my %rc = store_event();

        is %rc<result>, 'success', 'insert event';
        is %rc.<chaindetails>.elems, 2, '2 records in chaindetails';

        my $blockchaindetails = %rc.<chaindetails>.grep({ $_<transactionHash>.defined }).head;

        ok $blockchain_db.chainobj.wait_for_transactions(:hashes([$blockchaindetails<transactionHash>]), :attempts($tstobj.waittxiters)),
            sprintf('transaction %s is mined', $blockchaindetails<transactionHash>);

        diag(to-json(%rc));

        my @events = $ethelia.get_event_list;
        my $local_payload = from-json(MIME::Base64.new.decode-str(@events[0][3]));

        is $local_payload<enrichment><table>, $link, 'table enrichment';
        is $local_payload<enrichment><transactionHash>, $blockchaindetails<transactionHash>, 'transaction hash enrichment';
    }
    else {
        skip 'no Ethereum node', 5;
    }

    for @databases -> $dbobj {
        if $dbobj.dbswitch {
            $dbobj.chainobj.nonce = 0;
            ok $tstobj.drop_all(:dbobj($dbobj)), sprintf("drop table %s type %d", $dbobj.table, $dbobj.dbswitch);
        }
        else {
            todo 'Possible no Ethereum node';
            ok $dbobj.remove_all, sprintf("drop table %s type %d", $dbobj.table, $dbobj.dbswitch);
        }
    }
}, 'Check events storing to linked table on blockchain';

exit;

subtest {
    plan 3;

    my %rc = $ethelia.event_decode_api(
        :match({details => { path => '/api/ethelia/transaction/decode' }}),
        :credentials({token => '0xdece8b65318ed851f9224fccb297940ea858266ca3c2f29d200a2681ca2231b1'}),
        :tick(1),
        :sharedobj($ctrl.sharedobj),
        :payload({
            function  => 'set',
            inputdata => setraw
        }),
    );

    diag(%rc);

    ok %rc.keys && (%rc<tparams>:exists) && (%rc<tparams><transaction>:exists) && %rc<tparams><transaction>.keys, 'decoded transaction data';
    ok (%rc<tparams><transaction><rowdata>:exists) && %rc<tparams><transaction><rowdata>.keys, 'rowdata details';
    is %rc<tparams><transaction><rowdata><length>, 429, 'rowdata length';
}, 'Check transaction input data decoding';

done-testing;

sub store_event(UInt :$code = 1982, Str :$topic = 'sample_event', :$payload = payload) returns Hash {
    return $ethelia.event_store_api(
        :match({details => { path => '/api/ethelia/event/store' }}),
        :credentials({token => '0xdece8b65318ed851f9224fccb297940ea858266ca3c2f29d200a2681ca2231b1'}),
        :tick(1),
        :sharedobj($ctrl.sharedobj),
        :payload({
            code    => $code,
            topic   => $topic,
            payload => $payload
        }),
    );
}
