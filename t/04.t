use v6.d;
use Test;
use Test::Mock;

use Ethelia;
use Pheix::Addons::Embedded::Admin;
use Pheix::Controller::Basic;
use Pheix::Utils;

plan 1;

my $ctrl = mocked(
    Pheix::Controller::Basic,
    returning => {
        sharedobj => utilobj => Pheix::Utils.new,
        addons    =>
            'Pheix::Addons::Embedded::Admin' => {
                objct => Pheix::Addons::Embedded::Admin.new;
            }
    });

subtest {
    plan 6;

    my $ethelia = Ethelia.new(:confpath('custom-config/storage'));

    ok $ethelia, 'ethelia object';

    $ethelia  = $ethelia.get(:ctrl($ctrl));
    my %setup = $ethelia.jsonobj.get_entire_config;

    ok %setup<module><configuration><settings><accesstokens>:exists, 'config dump';

    my $token  = '0x57dea8c4e6a5d930be74a01b3d570ca157726cb2d7c1192bf20f76369c460470';
    my $routes = [ '/api/ethelia/event/store', '/api/ethelia/event/search' ];

    (my $date = DateTime.now(:timezone($*TZ)).Str) ~~ s:i/\.<[\d\+\:]>+$//;

    %setup<module><configuration><settings><accesstokens><group>{$token}<created> = $date;
    %setup<module><configuration><settings><accesstokens><group>{$token}<active>   = True;

    $ethelia.jsonobj.set_entire_config(:setup(%setup));

    my $code = 70474800019;

    my $payload = {
        username => 'John Doe',
        email => 'john@doe.com',
        message => "test message",
        public => $code.Str.substr(0, 6),
        private => Pheix::Utils.new(:jsonobj($ethelia.jsonobj)).do_encrypt(sprintf("%d:%d", $code, time)),
    };

    my $ret = $ethelia.validate_on_blockchain(:$token, :route($routes.tail), :$payload);

    ok $ret<status>, 'status';
    ok $ret<pkey>:exists, 'pkey';
    ok $ret<session>:exists && $ret<session> > 0, 'session';
    ok $ret<addr>:exists, 'address';

    diag($ret<session>) if $ret<session>;
}, 'Override auth on blockchain';

done-testing;
